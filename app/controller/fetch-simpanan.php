<?php
session_start();
include('../koneksi/koneksi.php');
if (isset($_SESSION['login_user']) AND $_SESSION['role'] === "Admin") {
    $sql = "SELECT 
                info.id_anggota, 
                simpan.id_transaksi, 
                info.nama_anggota, 
                simpan.nominal_simpanan, 
                simpan.tanggal, 
                simpan.jenis_simpanan, 
                simpan.status,
                simpan.bukti_transaksi  
            FROM info_anggota AS info INNER JOIN simpanan_anggota AS simpan ON info.id_anggota = simpan.id_anggota";
    $result = $conn->query($sql);

    $data = array();

    while($row = $result->fetch_assoc()) {
        $data[] = $row;
    }

    echo json_encode(array("data" => $data));
} elseif (isset($_SESSION['login_user']) AND $_SESSION['role'] === "Users") {
    $user_id = $_SESSION['login_user'];
    $sql = "SELECT 
                info.id_anggota,
                simpan.id_transaksi, 
                info.nama_anggota, 
                simpan.nominal_simpanan, 
                simpan.tanggal, 
                simpan.jenis_simpanan, 
                simpan.status,
                simpan.bukti_transaksi 
            FROM info_anggota AS info INNER JOIN simpanan_anggota AS simpan ON info.id_anggota = simpan.id_anggota WHERE info.id_anggota='$user_id'";
    $result = $conn->query($sql);

    $data = array();

    while($row = $result->fetch_assoc()) {
        $data[] = $row;
    }

    echo json_encode(array("data" => $data));
}
?>
