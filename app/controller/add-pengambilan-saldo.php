<?php

include('../koneksi/koneksi.php');

if ($_SERVER['REQUEST_METHOD'] == 'GET' AND isset($_GET['action']) == 'saldo') {
    try {
        $id_anggota = $_GET['id_anggota'];
        $query = "SELECT 
                    IFNULL(SUM(simpan.nominal_simpanan), 0) AS saldo_awal,
                    (SELECT SUM(jumlah_penarikan) FROM penarikan_saldo_anggota WHERE id_anggota='$id_anggota' AND status='approved') AS penarikan
                  FROM simpanan_anggota AS simpan 
                  WHERE simpan.id_anggota='$id_anggota' AND simpan.jenis_simpanan='Simpanan Sukarela' GROUP BY 
                  simpan.id_anggota";
        $result = $conn->query($query);
        $count = $result->fetch_assoc();
        $sisa_saldo = $count['saldo_awal'] - $count['penarikan'];
        $count['sisaSaldo'] = strval($sisa_saldo);
        echo json_encode($count);
    } catch (Exception $e) {
        echo $e->getMessage();
    } finally {
        $conn->close();
    }
} else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    try {
        $id_penarikan = $_POST['id_penarikan'];
        $id_anggota = $_POST['id_anggota'];
        $tgl = $_POST['tgl'];
        $simpanan_sukarela = $_POST['simpanan_sukarela'];
        $jumlah_penarikan= $_POST['jumlah_penarikan'];
        $status= $_POST['status'];
        $query = "INSERT INTO penarikan_saldo_anggota (id_transaksi, id_anggota, simpanan_sukarela, tanggal, jumlah_penarikan, status) VALUES ('$id_penarikan', '$id_anggota', '$simpanan_sukarela', '$tgl', '$jumlah_penarikan', '$status')";
            if ($conn->query($query) === TRUE) {
                $response['success'] = true;
                $response['message'] = "New record created successfully";
                echo json_encode($response);
            } else {
                $response['success'] = false;
                $response['message'] = "Error: " . $sql . "<br>" . $conn->error;
                echo json_encode($response);
            }
    } catch (Exception $e) {
        $response['success'] = false;
        $response['message'] = "Error: " . $e->getMessage() ."";
        echo json_encode($response);
    } finally {
        $conn->close();
    }
} elseif ($_SERVER['REQUEST_METHOD'] == 'PUT') {
    try {
        parse_str(file_get_contents("php://input"), $data);
        $id_penarikan = $data['id_penarikan'];
        $id_anggota = $data['id_anggota'];
        $tgl = $data['tgl'];
        $simpanan_sukarela = $data['simpanan_sukarela'];
        $jumlah_penarikan = $data['jumlah_penarikan'];
        $status = $data['status'];

        $sql = "UPDATE penarikan_saldo_anggota SET status='$status', tanggal='$tgl', simpanan_sukarela='$simpanan_sukarela', jumlah_penarikan='$jumlah_penarikan' WHERE id_transaksi='$id_penarikan' AND id_anggota='$id_anggota'";
        
            if ($conn->query($sql) === TRUE) {
                $response['success'] = true;
                $response['message'] = "New record updated successfully";
                echo json_encode($response);
            } else {
                $response['success'] = false;
                $response['message'] = "Error: " . $sql . "<br>" . $conn->error;
                echo json_encode($response);
            }
    } catch (Exception $e) {
        $response['success'] = false;
        $response['message'] = "Error: " . $e->getMessage() ."";
        echo json_encode($response);
    } finally {
        $conn->close();
    }
} elseif ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    try {
        parse_str(file_get_contents("php://input"), $data);
        $id_transaksi = $data['id_transaksi'];

        $sql = "DELETE FROM penarikan_saldo_anggota WHERE id_transaksi='$id_transaksi'";
    
            if ($conn->query($sql) === TRUE) {
                $response['success'] = true;
                $response['message'] = "Record deleted successfully";
                echo json_encode($response);
            } else {
                $response['success'] = false;
                $response['message'] = "Error: " . $sql . "<br>" . $conn->error . "";
                echo json_encode($response);
            }
    } catch (Exception $e) {
        $response['success'] = false;
        $response['message'] = "Error: " . $e->getMessage();
        echo json_encode($response);
    } finally {
        $conn->close();
    }
}