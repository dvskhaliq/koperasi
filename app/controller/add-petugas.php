<?php
include('../koneksi/koneksi.php');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    try {
        $id_anggota = $_POST['id_anggota'];
        $jabatan = $_POST['jabatan'];
        $username = $_POST['username'];
        $password = $_POST['password'];
    
        $sql = "INSERT INTO anggota (id_anggota, jabatan, username, password) VALUES ('$id_anggota', '$jabatan', '$username', '$password')";
    
        if ($conn->query($sql) === TRUE) {
            $response['success'] = true;
            $response['message'] = "New record created successfully";
            echo json_encode($response);
        } else {
            $response['success'] = false;
            $response['message'] = "Error: " . $sql . "<br>" . $conn->error . "";
            echo json_encode($response);
        }
    } catch (Exception $e) {
        $response['success'] = false;
        $response['message'] = "Error: " . $e->getMessage();
        echo json_encode($response);
    } finally {
        $conn->close();
    }

} elseif ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    try {
        parse_str(file_get_contents("php://input"), $data);
        $id_anggota = $data['id_anggota'];
    
        $sql = "DELETE FROM anggota WHERE id_anggota='$id_anggota'";
    
        if ($conn->query($sql) === TRUE) {
            $response['success'] = true;
            $response['message'] = "Record deleted successfully";
            echo json_encode($response);
        } else {
            $response['success'] = false;
            $response['message'] = "Error: " . $sql . "<br>" . $conn->error . "";
            echo json_encode($response);
        }
    } catch (Exception $e) {
        $response['success'] = false;
        $response['message'] = "Error: " . $e->getMessage();
        echo json_encode($response);
    } finally {
        $conn->close();
    }
} elseif ($_SERVER['REQUEST_METHOD'] == 'PUT') {
    try {
        parse_str(file_get_contents("php://input"), $data);
        $id_anggota = $data['id_anggota'];
        $jabatan = $data['jabatan'];
        $password = $data['password'];
    
        $sql = "UPDATE anggota SET jabatan='$jabatan', password='$password' WHERE id_anggota='$id_anggota'";
        if ($conn->query($sql) === TRUE) {
            $response['success'] = true;
            $response['message'] = "Record updated successfully";
            echo json_encode($response);
        } else {
            $response['success'] = false;
            $response['message'] = "Error: " . $sql . "<br>" . $conn->error . "";
            echo json_encode($response);
        }
    } catch (Exception $e) {
        $response['success'] = false;
        $response['message'] = "Error: " . $e->getMessage();
        echo json_encode($response);
    } finally {
        $conn->close();
    }
}
?>
