<?php
session_start();
include('../koneksi/koneksi.php');

// function feecthDateAngsuran($id_kredit, $id_anggota, $currentDate, $monthsToAdd) {
//     $allDates = [];
//     $dateNow = new DateTime($currentDate);
//     $months = $monthsToAdd;
//     $query = "SELECT tgl_kredit 
//               FROM angsuran_anggota 
//               WHERE id_kredit='$id_kredit' AND id_anggota='$id_anggota' AND status = 'success'";
//     $result = $conn->query($query);
//     $count = $result->fetch_assoc();
//     // Loop untuk setiap bulan dalam parameter
//     for ($i = 0; $i < $months; $i++) {
//         $date->modify('+1 month');
//         // Clone objek DateTime untuk menghindari modifikasi objek asli
//         $newDate = clone $date;
//         // Tambahkan tanggal ke array
//         $allDates[] = $newDate->format('Y-m-d');
//     }
//     return $allDates;
// }
if ($_SERVER['REQUEST_METHOD'] == 'GET' AND isset($_GET['param']) AND $_GET['param'] == 'initFetch') {
    try {
        $id_kredit = $_GET['id_kredit'];
        $id_anggota = $_GET['id_anggota'];
        $query = "SELECT 
                pjm.id_kredit, 
                pjm.id_anggota, 
                pjm.jumlah_pinjaman, 
                pjm.administrasi, 
                pjm.lama_cicilan,
                pjm.tanggal, 
                pjm.angsuran_perbulan,
                CASE
                    WHEN COUNT(angs.id_kredit) != pjm.lama_cicilan THEN COUNT(angs.id_kredit) + 1
                    ELSE 0
                END AS tenor_dibayar,
                ((pjm.jumlah_pinjaman + pjm.administrasi) - IFNULL(SUM(angs.nominal_pembayaran), 0)) AS sisa_angsuran
            FROM 
                pinjaman_anggota AS pjm 
            LEFT JOIN 
                angsuran_anggota AS angs 
            ON 
                pjm.id_kredit = angs.id_kredit 
                AND angs.status = 'success'
            WHERE 
                pjm.id_kredit='$id_kredit' AND pjm.id_anggota='$id_anggota'";
        $result = $conn->query($query);
        $count = $result->fetch_assoc();
        $currentDate = new DateTime($count['tanggal']);
        $monthsToAdd = 5;
        // $fetchDateAngsuran = feecthDateAngsuran($currentDate, $monthsToAdd);
        // print_r($fetchDateAngsuran);
        if ($result) {
            $response['success'] = true;
            $response['message'] = "New record created successfully";
            $response['data'] = $count;
            echo json_encode($response);
        } else {
            $response['success'] = false;
            $response['message'] = "Error: " . $query . "<br>" . $conn->error;
            $response['data'] = "";
            echo json_encode($response);
        }
    } catch (Exception $e) {
        $response['success'] = false;
        $response['message'] = "Error: " . $e->getMessage() ."";
        echo json_encode($response);
    } finally {
        $conn->close();
    }
} else if($_SERVER['REQUEST_METHOD'] == 'GET' AND isset($_GET['param']) AND $_GET['param'] == 'checkTempo') {
    $id_kredit = $_GET['id_kredit'];
    $id_anggota = $_GET['id_anggota'];
    $query = "SELECT 
                IFNULL(tgl_kredit, 0) AS tgl_kredit
              FROM angsuran_anggota 
              WHERE
                id_kredit='$id_kredit'
              AND 
                id_anggota='$id_anggota' AND status = 'success' ORDER BY tgl_kredit ASC";
    $result = $conn->query($query);
    $data = array();

    while($row = $result->fetch_assoc()) {
        $data[] = $row['tgl_kredit'];
    }
    echo json_encode(array("kredit_lunas" => $data));
    // print_r($data);
} else if($_SERVER['REQUEST_METHOD'] == 'POST') {
    try {
        $actions = $_POST['action'];
        $id_angsuran = $_POST['id_angsuran'];
        $id_kredit = $_POST['id_kredit'];
        $id_anggota = $_POST['id_anggota'];
        $nominal_angsuran = $_POST['nominal_angsuran'];
        $tgl_kredit = $_POST['tgl_kredit'];
        $tgl_bayar = $_POST['tgl_bayar'];
        $denda = $_POST['denda'];
        $total_bayar = $_POST['total_bayar'];
        $terlambat = $_POST['terlambat'];
        $keterangan = $_POST['keterangan'];
        $tenor_bulan = $_POST['tenor_bulan'];
        $status = $_POST['status'];
        if ($actions == 'post') {
            if (isset($_FILES['bukti_transaksi'])) {
                $bukti_transaksi_Tmp = $_FILES['bukti_transaksi']['tmp_name'];
                $bukti_transaksi_Name = $_FILES['bukti_transaksi']['name'];
                $bukti_transaksi_type = $_FILES['bukti_transaksi']['type'];
                $bukti_transaksi_size = $_FILES['bukti_transaksi']['size'];
                $ext_bukti_transaksi = strtolower(pathinfo($bukti_transaksi_Name,PATHINFO_EXTENSION));
                $extImgValid = ['jpg', 'jpeg', 'png'];
                if ($bukti_transaksi_size < 1000000) {
                    if (in_array($ext_bukti_transaksi, $extImgValid)) {
                        $uploadDir = '../../assets/image/uploads/';
                        $uploadFile = $uploadDir . basename($bukti_transaksi_Name);
                        if (move_uploaded_file($bukti_transaksi_Tmp, $uploadFile)) {
                            $file_name = $bukti_transaksi_Name;
                            $query = "INSERT INTO angsuran_anggota (
                                id_angsuran,
                                id_kredit, 
                                id_anggota, 
                                nominal_pembayaran, 
                                tgl_kredit,
                                tanggal_bayar, 
                                denda, 
                                total_pembayaran, 
                                terlambat, 
                                status, 
                                tenor_bulan, 
                                keterangan,
                                bukti_transaksi
                            ) VALUES (
                            '$id_angsuran',
                            '$id_kredit', 
                            '$id_anggota', 
                            '$nominal_angsuran',
                            '$tgl_kredit',
                            '$tgl_bayar', 
                            '$denda', 
                            '$total_bayar', 
                            '$terlambat', 
                            '$status', 
                            '$tenor_bulan', 
                            '$keterangan',
                            '$file_name')";
                            if ($conn->query($query) === TRUE) {
                                $response['success'] = true;
                                $response['message'] = "New record created successfully";
                                echo json_encode($response);
                            } else {
                                $response['success'] = false;
                                $response['message'] = "Error: " . $sql . "<br>" . $conn->error;
                                echo json_encode($response);
                            }
                        } else {
                            $response['success'] = false;
                            $response['message'] = "Terjadi kesalahan saat mengupload file.";
                            echo json_encode($response);
                        }
                    } else {
                        $response['success'] = false;
                        $response['message'] = "Mohon mengupload hanya file gambar saja.";
                        echo json_encode($response);
                    }
                } else {
                    $response['success'] = false;
                    $response['message'] = "Size file yang di upload maksimal 1MB.";
                    echo json_encode($response);
                }
            } else {
                $response['success'] = false;
                $response['message'] = "Mohon untuk mengupload file bukti transaksi.";
                echo json_encode($response);
            }
        } else if ($actions == 'put') {
            if (isset($_FILES['bukti_transaksi'])) {
                $bukti_transaksi_Tmp = $_FILES['bukti_transaksi']['tmp_name'];
                $bukti_transaksi_Name = $_FILES['bukti_transaksi']['name'];
                $bukti_transaksi_type = $_FILES['bukti_transaksi']['type'];
                $bukti_transaksi_size = $_FILES['bukti_transaksi']['size'];
                $ext_bukti_transaksi = strtolower(pathinfo($bukti_transaksi_Name,PATHINFO_EXTENSION));
                $extImgValid = ['jpg', 'jpeg', 'png'];
                if ($bukti_transaksi_size < 1000000) {
                    if (in_array($ext_bukti_transaksi, $extImgValid)) {
                        $uploadDir = '../../assets/image/uploads/';
                        $uploadFile = $uploadDir . basename($bukti_transaksi_Name);
                        // CEK Existing File 
                        $sql  = "SELECT bukti_transaksi FROM angsuran_anggota WHERE id_angsuran='$id_angsuran' AND id_kredit='$id_kredit'";
                        $result = $conn->query($sql);
                        $files = $result->fetch_assoc();
                        $existing_file = $uploadDir . $files['bukti_transaksi'];
                        if (file_exists($existing_file)) { 
                            unlink($existing_file);
                            move_uploaded_file($bukti_transaksi_Tmp, $uploadFile);
                        }
                        $file_name = $bukti_transaksi_Name;
                        $query = "UPDATE angsuran_anggota
                                    SET 
                                        keterangan = '$keterangan',
                                        status = '$status',
                                        bukti_transaksi = '$file_name'
                                  WHERE id_angsuran='$id_angsuran' AND id_kredit='$id_kredit'";
                        if ($conn->query($query) === TRUE) {
                            $response['success'] = true;
                            $response['message'] = "record successfully updated";
                            echo json_encode($response);
                        } else {
                            $response['success'] = false;
                            $response['message'] = "Error: " . $sql . "<br>" . $conn->error;
                            echo json_encode($response);
                        }
                    }
                } else {
                    $response['success'] = false;
                    $response['message'] = "Size file yang di upload maksimal 1MB.";
                    echo json_encode($response);
                }
            } else {
                $query = "UPDATE angsuran_anggota
                          SET 
                            keterangan = '$keterangan',
                            status = '$status'
                          WHERE id_angsuran='$id_angsuran' AND id_kredit='$id_kredit'";
                if ($conn->query($query) === TRUE) {
                    $response['success'] = true;
                    $response['message'] = "record successfully updated";
                    echo json_encode($response);
                } else {
                    $response['success'] = false;
                    $response['message'] = "Error: " . $sql . "<br>" . $conn->error;
                    echo json_encode($response);
                }
            }
        }
    } catch (Exception $e) {
        $response['success'] = false;
        $response['message'] = "Error: " . $e->getMessage() ."";
        echo json_encode($response);
    } finally {
        $conn->close();
    }
} else if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    try {
        parse_str(file_get_contents("php://input"), $data);
        $id_kredit = $data['id_kredit'];
        $sql = "DELETE FROM angsuran_anggota WHERE id_kredit='$id_kredit'";
    
        if ($conn->query($sql) === TRUE) {
            $response['success'] = true;
            $response['message'] = "Record deleted successfully";
            echo json_encode($response);
        } else {
            $response['success'] = false;
            $response['message'] = "Error: " . $sql . "<br>" . $conn->error . "";
            echo json_encode($response);
        }
    } catch (Exception $e) {
        $response['success'] = false;
        $response['message'] = "Error: " . $e->getMessage();
        echo json_encode($response);
    } finally {
        $conn->close();
    }
}