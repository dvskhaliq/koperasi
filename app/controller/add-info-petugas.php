<?php
include('../koneksi/koneksi.php');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    try {
        // Mengambil data anggota
        $query = "SELECT id_anggota, username FROM anggota";
        $result = $conn->query($query);

        $anggotaList = array();
        while ($row = $result->fetch_assoc()) {
            $anggotaList[] = $row;
        }

        echo json_encode($anggotaList);
    } catch (Exception $e) {
        echo $e->getMessage();
    } finally {
        $conn->close();
    }

} elseif ($_SERVER['REQUEST_METHOD'] == 'POST') { 
    try {
        $id_anggota = $_POST['id_anggota'];
        $namaAnggota = $_POST['namaAnggota'];
        $tglLahir = $_POST['tglLahir'];
        $kotaLahir = $_POST['kotaLahir'];
        $tlp = $_POST['tlp'];
        $address = $_POST['address'];

        $query = "SELECT COUNT(*) AS Total FROM info_anggota WHERE id_anggota = '$id_anggota'";
        $result = $conn->query($query);
        $count = $result->fetch_assoc();

        if ($count['Total'] > 0) {
            $response['success'] = false;
            $response['message'] = "Informasi Anggota sudah ada, silahkan lakukan edit";
            echo json_encode($response);
        } else {
            $sql = "INSERT INTO info_anggota (
                        id_anggota,
                        nama_anggota,
                        alamat,
                        kota_lahir,
                        tanggal_lahir,
                        telpon
                    ) VALUES ('$id_anggota', '$namaAnggota', '$address', '$kotaLahir', '$tglLahir', '$tlp')";
    
            if ($conn->query($sql) === TRUE) {
                $response['success'] = true;
                $response['message'] = "New record created successfully";
                echo json_encode($response);
            } else {
                $response['success'] = false;
                $response['message'] = "Error: " . $sql . "<br>" . $conn->error;
                echo json_encode($response);
            }
        }
    } catch (Exception $e) {
        echo $e->getMessage();
    } finally {
        $conn->close();
    }
} elseif ($_SERVER['REQUEST_METHOD'] == 'PUT') { 
    try {
        parse_str(file_get_contents("php://input"), $data);
        $id_anggota = $data['id_anggota'];
        $namaAnggota = $data['namaAnggota'];
        $tglLahir = $data['tglLahir'];
        $kotaLahir = $data['kotaLahir'];
        $tlp = $data['tlp'];
        $address = $data['address'];

        $sql = "UPDATE info_anggota SET nama_anggota='$namaAnggota', alamat='$address', kota_lahir='$kotaLahir', tanggal_lahir='$tglLahir', telpon='$tlp'  WHERE id_anggota='$id_anggota'";
        if ($conn->query($sql) === TRUE) {
            $response['success'] = true;
            $response['message'] = "Record updated successfully";
            echo json_encode($response);
        } else {
            $response['success'] = false;
            $response['message'] = "Error: " . $sql . "<br>" . $conn->error . "";
            echo json_encode($response);
        }
    } catch (Exception $e) {
        $response['success'] = false;
        $response['message'] = "Error: " . $e->getMessage();
        echo json_encode($response);
    } finally {
        $conn->close();
    }
} elseif ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    try {
        parse_str(file_get_contents("php://input"), $data);
        $id_anggota = $data['id_anggota'];
    
        $sql = "DELETE FROM info_anggota WHERE id_anggota='$id_anggota'";
    
        if ($conn->query($sql) === TRUE) {
            echo "Record deleted successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    } catch (Exception $e) {
        echo $e->getMessage();
    } finally {
        $conn->close();
    }
}