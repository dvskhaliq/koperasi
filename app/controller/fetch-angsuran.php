<?php
include('../koneksi/koneksi.php');

if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['kredit']) && isset($_GET['anggota'])) { 
    try {
        // Mengambil data anggota
        $id_kredit = $_GET['kredit'];
        $id_anggota = $_GET['anggota'];
        $queryCount = "SELECT 
                        COUNT(*) as total
                       FROM 
                        angsuran_anggota 
                       WHERE 
                        id_kredit='$id_kredit' AND id_anggota='$id_anggota'";
        $result = mysqli_query($conn, $queryCount);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        if ($row['total'] > 0) {
            $query = "SELECT
                        pjm.id_kredit, 
                        pjm.id_anggota,
                        angs.id_angsuran, 
                        anggota.nama_anggota, 
                        pjm.jumlah_pinjaman, 
                        pjm.administrasi, 
                        angs.nominal_pembayaran,
                        angs.terlambat,
                        angs.denda,
                        angs.tanggal_bayar, 
                        angs.tgl_kredit, 
                        angs.total_pembayaran,
                        angs.status,
                        angs.tenor_bulan,
                        angs.bukti_transaksi,
                        ((pjm.jumlah_pinjaman + pjm.administrasi) - IFNULL(SUM(angs.nominal_pembayaran), 0)) AS sisa_angsuran,
                        angs.keterangan
                    FROM 
                        pinjaman_anggota AS pjm 
                    INNER JOIN
                        info_anggota as anggota
                    ON pjm.id_anggota=anggota.id_anggota
                    INNER JOIN 
                        angsuran_anggota AS angs 
                    ON pjm.id_kredit = angs.id_kredit 
                    WHERE pjm.id_kredit='$id_kredit' AND pjm.id_anggota='$id_anggota'";

            $result = $conn->query($query);
            $data = array();
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }

            echo json_encode(array("data" => $data));
        } else {
            echo json_encode(array("data" => []));
        }
    } catch (Exception $e) {
        echo $e->getMessage();
    } finally {
        $conn->close();
    }
    
}