<?php
include('../koneksi/koneksi.php');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    try {
        $id_anggota = $_POST['id_anggota'];
        $id_transaksi = $_POST['id_transaksi'];
        $nominal = $_POST['nominal'];
        $tgl = $_POST['tgl'];
        $jenis = $_POST['jenis'];
        if (isset($_FILES['bukti_transaksi'])) {
            $bukti_transaksi_Tmp = $_FILES['bukti_transaksi']['tmp_name'];
            $bukti_transaksi_Name = $_FILES['bukti_transaksi']['name'];
            $bukti_transaksi_type = $_FILES['bukti_transaksi']['type'];
            $bukti_transaksi_size = $_FILES['bukti_transaksi']['size'];
            $ext_bukti_transaksi = strtolower(pathinfo($bukti_transaksi_Name,PATHINFO_EXTENSION));
            $extImgValid = ['jpg', 'jpeg', 'png'];
            if ($bukti_transaksi_size < 1000000) {
                if (in_array($ext_bukti_transaksi, $extImgValid)) {
                    $uploadDir = '../../assets/image/uploads/';
                    $uploadFile = $uploadDir . basename($bukti_transaksi_Name);
                    if (move_uploaded_file($bukti_transaksi_Tmp, $uploadFile)) {
                        $file_name = $bukti_transaksi_Name;
                        $sql = "INSERT INTO simpanan_anggota (id_transaksi, id_anggota, nominal_simpanan, tanggal, jenis_simpanan, status, bukti_transaksi) VALUES ('$id_transaksi', '$id_anggota', '$nominal', '$tgl', '$jenis', 'pending', '$file_name')";
    
                        if ($conn->query($sql) === TRUE) {
                            $response['success'] = true;
                            $response['message'] = "New record created successfully";
                            echo json_encode($response);
                        } else {
                            $response['success'] = false;
                            $response['message'] = "Error: " . $sql . "<br>" . $conn->error;
                            echo json_encode($response);
                        }
                    } else {
                        $response['success'] = false;
                        $response['message'] = "Terjadi kesalahan saat mengupload file.";
                        echo json_encode($response);
                    }
                } else {
                    $response['success'] = false;
                    $response['message'] = "Mohon mengupload hanya file gambar saja.";
                    echo json_encode($response);
                }
            } else {
                $response['success'] = false;
                $response['message'] = "Size file yang di upload maksimal 1MB.";
                echo json_encode($response);
            }
        } else {
            $response['success'] = false;
            $response['message'] = "Mohon untuk mengupload file bukti transaksi.";
            echo json_encode($response);
        }
    } catch (Exception $e) {
        $response['success'] = false;
        $response['message'] = "Error: " . $e->getMessage() ."";
        echo json_encode($response);
    } finally {
        $conn->close();
    }

} elseif ($_SERVER['REQUEST_METHOD'] == 'PUT') {
    try {
        parse_str(file_get_contents("php://input"), $data);
        $id_anggota = $data['id_anggota'];
        $id_transaksi = $data['id_transaksi'];
        $status = $data['status'];
        $nominal = $data['nominal'];
        $tgl = $data['tgl'];
        $jenis = $data['jenis'];

        $sql = "UPDATE simpanan_anggota SET nominal_simpanan='$nominal', tanggal='$tgl', jenis_simpanan='$jenis', status='$status' WHERE id_transaksi='$id_transaksi' AND id_anggota='$id_anggota'";
    
        if ($conn->query($sql) === TRUE) {
            $response['success'] = true;
            $response['message'] = "New record updated successfully";
            echo json_encode($response);
        } else {
            $response['success'] = false;
            $response['message'] = "Error: " . $sql . "<br>" . $conn->error;
            echo json_encode($response);
        }
    } catch (Exception $e) {
        $response['success'] = false;
        $response['message'] = "Error: " . $e->getMessage() ."";
        echo json_encode($response);
    } finally {
        $conn->close();
    }
} elseif ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    try {
        parse_str(file_get_contents("php://input"), $data);
        $id_transaksi = $data['id_transaksi'];
    
        $sql = "DELETE FROM simpanan_anggota WHERE id_transaksi='$id_transaksi'";
    
        if ($conn->query($sql) === TRUE) {
            $response['success'] = true;
            $response['message'] = "Record deleted successfully";
            echo json_encode($response);
        } else {
            $response['success'] = false;
            $response['message'] = "Error: " . $sql . "<br>" . $conn->error . "";
            echo json_encode($response);
        }
    } catch (Exception $e) {
        $response['success'] = false;
        $response['message'] = "Error: " . $e->getMessage();
        echo json_encode($response);
    } finally {
        $conn->close();
    }
}