<?php
include('../koneksi/koneksi.php');

if ($_SERVER['REQUEST_METHOD'] == 'GET' AND isset($_GET['action']) == 'saldo') {
    try {
        $id_anggota = $_GET['id_anggota'];
        $query = "SELECT
                    info.alamat,
                    info.kota_lahir,
                    info.tanggal_lahir,
                    info.telpon, 
                    info.alamat,
                    (
                    IFNULL(SUM(simpan.nominal_simpanan), 0) -
                        (
                            SELECT
                                (SUM((pinjam.jumlah_pinjaman + pinjam.administrasi)) - IFNULL(SUM(angsuran.nominal_pembayaran), 0))
                            FROM pinjaman_anggota AS pinjam
                                LEFT JOIN angsuran_anggota AS angsuran 
                                ON pinjam.id_kredit=angsuran.id_kredit
                                AND angsuran.status='success' 
                            WHERE pinjam.status='approved' AND pinjam.id_anggota='$id_anggota'
                        )
                    ) AS saldo
                  FROM info_anggota AS info LEFT JOIN simpanan_anggota AS simpan
                  ON info.id_anggota=simpan.id_anggota
                  WHERE info.id_anggota='$id_anggota' AND simpan.jenis_simpanan='Simpanan Wajib'";
        $result = $conn->query($query);
        $count = $result->fetch_assoc(); 
        echo json_encode($count);
    } catch (Exception $e) {
        echo $e->getMessage();
    } finally {
        $conn->close();
    }
} elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
    try {
        $id_kredit = $_POST['id_kredit'];
        $id_anggota = $_POST['id_anggota'];
        $nik = $_POST['nik'];
        $telpon = $_POST['telpon'];
        $kota_lahir = $_POST['kota_lahir'];
        $tanggal_lahir = $_POST['tanggal_lahir'];
        $alamat = $_POST['alamat'];
        $tgl = $_POST['tgl'];
        $saldo = $_POST['saldo'];
        $jumlah_pinjaman = $_POST['jumlah_pinjaman'];
        $admin = $_POST['admin'];
        $tenor = $_POST['tenor'];
        $angsuran = $_POST['angsuran'];
        $status = $_POST['status'];
        $query = "INSERT INTO pinjaman_anggota (
                    id_kredit, 
                    tanggal, 
                    id_anggota, 
                    nik,
                    telpon,
                    kota_lahir,
                    tanggal_lahir,
                    alamat,
                    saldo, 
                    jumlah_pinjaman, 
                    administrasi, 
                    lama_cicilan, 
                    angsuran_perbulan,
                    status) 
                  VALUES (
                   '$id_kredit',
                   '$tgl', 
                   '$id_anggota', 
                   '$nik',
                   '$telpon',
                   '$kota_lahir',
                   '$tanggal_lahir',
                   '$alamat',
                   '$saldo', 
                   '$jumlah_pinjaman', 
                   '$admin', 
                   '$tenor', 
                   '$angsuran',
                   '$status')";
        if ($conn->query($query) === TRUE) {
            $response['success'] = true;
            $response['message'] = "New record created successfully";
            echo json_encode($response);
        } else {
            $response['success'] = false;
            $response['message'] = "Error: " . $sql . "<br>" . $conn->error;
            echo json_encode($response);
        }
    } catch (Exception $e) {
        $response['success'] = false;
        $response['message'] = "Error: " . $e->getMessage() ."";
        echo json_encode($response);
    } finally {
        $conn->close();
    }
} elseif ($_SERVER['REQUEST_METHOD'] == 'PUT') {
    try {
        parse_str(file_get_contents("php://input"), $data);
        $id_kredit = $data['id_kredit'];
        $id_anggota = $data['id_anggota'];
        $tgl = $data['tgl'];
        $saldo = $data['saldo'];
        $jumlah_pinjaman = $data['jumlah_pinjaman'];
        $admin = $data['admin'];
        $tenor = $data['tenor'];
        $angsuran = $data['angsuran'];
        $status = $data['status'];
    
        $sql = "UPDATE pinjaman_anggota SET tanggal='$tgl', saldo='$saldo', jumlah_pinjaman='$jumlah_pinjaman', administrasi='$admin', lama_cicilan='$tenor', angsuran_perbulan='$angsuran', status='$status' WHERE id_kredit='$id_kredit' AND id_anggota='$id_anggota'";
        
            if ($conn->query($sql) === TRUE) {
                $response['success'] = true;
                $response['message'] = "New record updated successfully";
                echo json_encode($response);
            } else {
                $response['success'] = false;
                $response['message'] = "Error: " . $sql . "<br>" . $conn->error;
                echo json_encode($response);
            }
    } catch (Exception $e) {
        $response['success'] = false;
        $response['message'] = "Error: " . $e->getMessage() ."";
        echo json_encode($response);
    } finally {
        $conn->close();
    }
} elseif ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    parse_str(file_get_contents("php://input"), $data);
    $id_kredit = $data['id_kredit'];

    $sql = "DELETE FROM pinjaman_anggota WHERE id_kredit='$id_kredit'";
    
        if ($conn->query($sql) === TRUE) {
            $response['success'] = true;
            $response['message'] = "Record deleted successfully";
            echo json_encode($response);
        } else {
            $response['success'] = false;
            $response['message'] = "Error: " . $sql . "<br>" . $conn->error . "";
            echo json_encode($response);
        }
}

