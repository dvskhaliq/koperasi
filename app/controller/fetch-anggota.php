<?php
session_start();
include('../koneksi/koneksi.php');

if ($_SERVER['REQUEST_METHOD'] == 'GET') { 

    if (isset($_SESSION['login_user']) AND $_SESSION['role'] === "Admin") {
        try {
            // Mengambil data anggota
            $query = "SELECT agt.id_anggota, info.nama_anggota FROM anggota as agt INNER JOIN info_anggota as info ON agt.id_anggota=info.id_anggota";
            $result = $conn->query($query);
    
            $anggotaList = array(); 
            while ($row = $result->fetch_assoc()) {
                $anggotaList[] = $row;
            }
    
            echo json_encode($anggotaList);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $conn->close();
        }
    } elseif (isset($_SESSION['login_user']) AND $_SESSION['role'] === "Users") {
        try {
            //code...
            $user_id = $_SESSION['login_user'];
            $query = "SELECT 
                        agt.id_anggota, 
                        info.nama_anggota 
                      FROM anggota as agt 
                      INNER JOIN info_anggota as info 
                      ON agt.id_anggota=info.id_anggota WHERE agt.id_anggota='$user_id'";
            $result = $conn->query($query);
    
            $anggotaList = array(); 
            while ($row = $result->fetch_assoc()) {
                $anggotaList[] = $row;
            }
    
            echo json_encode($anggotaList);
        } catch (Exception $e) {
            echo $e->getMessage();
        } finally {
            $conn->close();
        }
    }
}