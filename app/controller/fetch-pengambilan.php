<?php
session_start();
include('../koneksi/koneksi.php');
if (isset($_SESSION['login_user']) AND $_SESSION['role'] === "Admin") {
    $sql = "SELECT 
                penarikan.id_transaksi,
                penarikan.id_anggota,
                info.nama_anggota,
                penarikan.tanggal, 
                penarikan.simpanan_sukarela, 
                penarikan.jumlah_penarikan,
                penarikan.status 
            FROM penarikan_saldo_anggota AS penarikan 
            INNER JOIN info_anggota AS info ON penarikan.id_anggota=info.id_anggota 
            INNER JOIN simpanan_anggota AS simpan ON penarikan.id_anggota = simpan.id_anggota AND simpan.jenis_simpanan='Simpanan Sukarela'";
    $result = $conn->query($sql);

    $data = array();

    while($row = $result->fetch_assoc()) {
        $data[] = $row;
    }

    echo json_encode(array("data" => $data));
} elseif (isset($_SESSION['login_user']) AND $_SESSION['role'] === "Users") {
    $user_id = $_SESSION['login_user'];
    $sql = "SELECT 
                penarikan.id_transaksi,
                penarikan.id_anggota,
                info.nama_anggota,
                penarikan.tanggal,
                penarikan.simpanan_sukarela, 
                penarikan.jumlah_penarikan,
                penarikan.status  
            FROM penarikan_saldo_anggota AS penarikan 
            INNER JOIN info_anggota AS info ON penarikan.id_anggota=info.id_anggota 
            INNER JOIN simpanan_anggota AS simpan ON penarikan.id_anggota = simpan.id_anggota AND simpan.jenis_simpanan='Simpanan Sukarela' 
            WHERE penarikan.id_anggota='$user_id'";
    $result = $conn->query($sql);

    $data = array();

    while($row = $result->fetch_assoc()) {
        $data[] = $row;
    }

    echo json_encode(array("data" => $data));
}