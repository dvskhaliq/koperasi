<?php
session_start();
include('../koneksi/koneksi.php');

if (isset($_SESSION['login_user']) AND $_SESSION['role'] === "Admin") {
    $sql = "SELECT 
                pinjam.id_kredit, 
                info.id_anggota, 
                info.nama_anggota,
                pinjam.nik,
                pinjam.telpon,
                pinjam.kota_lahir,
                pinjam.tanggal_lahir,
                pinjam.alamat, 
                pinjam.tanggal, 
                pinjam.saldo, 
                pinjam.jumlah_pinjaman, 
                pinjam.administrasi,
                pinjam.lama_cicilan, 
                pinjam.angsuran_perbulan,
                pinjam.status
            FROM info_anggota AS info INNER JOIN pinjaman_anggota AS pinjam 
            ON info.id_anggota = pinjam.id_anggota";
    $result = $conn->query($sql);

    $data = array();

    while($row = $result->fetch_assoc()) {
        $data[] = $row;
    } 

    echo json_encode(array("data" => $data));
} elseif (isset($_SESSION['login_user']) AND $_SESSION['role'] === "Users") {
    $user_id = $_SESSION['login_user'];
    $sql = "SELECT 
                pinjam.id_kredit, 
                info.id_anggota, 
                info.nama_anggota,
                pinjam.nik,  
                pinjam.telpon,
                pinjam.kota_lahir,
                pinjam.tanggal_lahir,
                pinjam.alamat,
                pinjam.tanggal, 
                pinjam.saldo, 
                pinjam.jumlah_pinjaman,
                pinjam.administrasi, 
                pinjam.lama_cicilan, 
                pinjam.angsuran_perbulan,
                pinjam.status
            FROM info_anggota AS info INNER JOIN pinjaman_anggota AS pinjam 
            ON info.id_anggota = pinjam.id_anggota WHERE info.id_anggota='$user_id'";
    $result = $conn->query($sql);

    $data = array();

    while($row = $result->fetch_assoc()) {
        $data[] = $row;
    } 

    echo json_encode(array("data" => $data));
}
?>
