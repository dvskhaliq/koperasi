<?php
session_start();

// Memeriksa apakah session user login ada
if (isset($_SESSION['login_user']) AND $_SESSION['role'] === 'Admin') {
    echo json_encode(['loggedIn' => true, 'role' => 'Admin']);
}elseif(isset($_SESSION['login_user']) AND $_SESSION['role'] === 'Users'){
    echo json_encode(['loggedIn' => true, 'role' => 'Users']);
}else {
    echo json_encode(['loggedIn' => false]);
}
?>