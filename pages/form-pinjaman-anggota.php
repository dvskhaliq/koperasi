<?php
session_start();
if (!isset($_SESSION['login_user'])) {
    header("Location: ../index.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Petugas</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- DataTables CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css">
    <!-- Sweetalert -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="../assets/css/pinjam-pages.css">
</head>
<body>
<?php require_once('template/navigation.php') ?>
    <div class="container-fluid">
        <div class="content mt-5">
            <div class="row">
                <div class="card mx-auto" style="width: 50rem;">
                    <div class="card-body" style="padding: 20px; margin: 20px;">
                        <form id="petugasForm">
                            <div class="form-group">
                                <input type="hidden" id="action" name="action" value="post">
                            </div>
                            <div class="form-group">
                                <input type="hidden" id="status" name="status" value="pending">
                            </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="id_anggota">Id Kredit</label>
                                            <input type="text" class="form-control" id="id_kredit" name="id_kredit" readonly>
                                        </div>
                                        <div class="form-group" id="nama-anggota">
                                            <label for="jabatan">Nama Lengkap Anggota</label>
                                            <select class="form-control" id="name_anggota" name="name_anggota" required>
                                            <!-- Options akan diisi menggunakan jQuery -->
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nominal">NIK</label>
                                            <input type="text" class="form-control" id="nik" name="nik" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="nominal">Nomor Telphone</label>
                                            <input type="text" class="form-control" id="tlp" name="tlp" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="nominal">Kota Lahir</label>
                                            <input type="text" class="form-control" id="kotaLahir" name="kotaLahir" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="tanggal">Tanggal Lahir</label>
                                            <input type="date" class="form-control" id="tglLahir" name="tglLahir" readonly>
                                        </div> 
                                        <div class="form-group">
                                            <label for="username">Alamat</label>
                                            <textarea type="text" class="form-control" id="address" name="address" readonly></textarea>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="nominal">Saldo</label>
                                            <input type="text" class="form-control" id="saldo" name="saldo" value="0" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="tanggal">Tanggal Pengajuan</label>
                                            <input type="date" class="form-control" id="tgl" name="tgl">
                                        </div>  
                                        <div class="form-group">
                                            <label for="nominal">Jumlah Pinjaman</label>
                                            <input type="text" class="form-control" id="jumlah_pinjaman" value="0" name="jumlah_pinjaman">
                                        </div>
                                        <div class="form-group">
                                            <label for="jenis">Administrasi</label>
                                            <input type="text" class="form-control" id="admin" name="admin" value="0" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="jenis">Lama Cicilan (Tenor)</label>
                                            <input type="text" class="form-control" id="tenor" name="tenor" value="0">
                                        </div>
                                        <div class="form-group">
                                            <label for="jenis">Angsuran/Bulan</label>
                                            <input type="text" class="form-control" id="angsuran" name="angsuran" value="0" readonly>
                                        </div>
                                    </div>
                                </div>
                            <button type="button" class="btn btn-danger" id="btnBatal">Batal</button>
                            <button type="button" class="btn btn-primary" id="btnSimpan">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->
    <!-- DataTables JS -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <!-- Sweetalert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- jQuery Mask Plugin -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <!-- Custom JS -->
    <script src="../assets/js/form-pengajuan.js"></script>
</body>
</html>
