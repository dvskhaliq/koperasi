<?php
session_start();
// echo "cek update proses from admin";
// include('../koneksi/koneksi.php');
if (!isset($_SESSION['login_user'])) {
    header("Location: ../index.php");
    exit(); 
} else {
    if (isset($_GET['kredit']) AND isset($_GET['anggota'])) {
        $id_kredit = $_GET['kredit'];
        $id_anggota = $_GET['anggota'];

    } else {
        header("Location: pinjaman-anggota.php");
        exit();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Petugas</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- DataTables CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css">
    <!-- Sweetalert -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="../assets/css/pinjam-pages.css">
</head>
<body>
<?php require_once('template/navigation.php') ?>
    <div class="container-fluid">
        <div class="content mt-5">
            <div class="row">
                <div class="card mx-auto" style="width: 50rem;">
                    <div class="card-body" style="padding: 20px; margin: 20px;">
                        <form id="petugasForm">
                            <div class="form-group">
                                <input type="hidden" id="action" name="action" value="post">
                            </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="id_anggota">Id Angsuran</label>
                                            <input type="text" class="form-control" id="id_angsuran" name="id_angsuran" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_anggota">Id Kredit</label>
                                            <input type="text" class="form-control" id="id_kredit" name="id_kredit" value="<?= $id_kredit; ?>" readonly>
                                        </div>
                                        <div class="form-group" id="field-idanggota">
                                            <label id="labelJabatan" for="jabatan">Id Anggota</label>
                                            <input type="text" class="form-control" id="id_anggota" name="id_anggota" value="<?= $id_anggota ?>" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="tanggal">Tanggal Jatuh Tempo</label>
                                            <input type="date" class="form-control" id="tgl_kredit" name="tgl_kredit" readonly>
                                        </div> 
                                        <div class="form-group">
                                            <label for="nominal">Jumlah Pinjaman</label>
                                            <input type="text" class="form-control" id="jumlah_pinjaman" name="jumlah_pinjaman" value="0" readonly>
                                        </div> 
                                        <div class="form-group">
                                            <label for="tanggal">Tanggal Bayar</label>
                                            <input type="date" class="form-control" id="tgl" name="tgl">
                                        </div>                    
                                        <div class="form-group">
                                            <label for="ket">Keterangan</label>
                                            <textarea class="form-control" id="keterangan" name="keterangan" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="nominal">Nominal Pembayaran Angsuran</label>
                                            <input type="text" class="form-control" id="nominal_angsuran" value="0" name="nominal_angsuran" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="nominal">Denda Keterlambatan</label>
                                            <input type="text" class="form-control" id="denda" value="0" name="denda" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="nominal">Total Pembayaran</label>
                                            <input type="text" class="form-control" id="total_bayar" value="0" name="total_bayar" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="nominal">Sisa Pembayaran</label>
                                            <input type="text" class="form-control" id="sisa_bayar" value="0" name="sisa_bayar" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="nominal">Tenor Pembayaran</label>
                                            <div class="row">
                                                <div class="col-sm-2"><input type="text" class="form-control" style="width: 60px;" id="tenor_dibayar" value="0" name="tenor_dibayar" readonly></div>
                                                <div class="col"><p style="margin: 10px 50px 0 0;"> Bulan</p></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nominal">Keterlambatan Pembayaran</label>
                                            <div class="row">
                                                <div class="col-sm-2"><input type="number" class="form-control" style="width: 60px;" id="terlambat" value="0" name="terlambat" readonly></div>
                                                <div class="col"><p style="margin: 10px 50px 0 0;"> Hari</p></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tanggal">Bukti Transaksi</label>
                                            <div class="input-group mb-3">
                                                <input type="file" name="file" id="file" required>
                                            </div>
                                        </div>
                                        <?php if (isset($_SESSION['role']) AND $_SESSION['role'] === 'Admin') {?>
                                        <div class="form-group">
                                            <label for="jenis">Status</label>
                                            <select class="form-control" id="status" name="status" required>
                                                <option>Silahkan pilih status</option>
                                                <option value="pending">Pending</option>
                                                <option value="approved">Approved</option>
                                                <option value="rejected">Rejected</option>
                                            </select>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <button type="button" class="btn btn-danger" id="btnBatal">Batal</button>
                            <button type="button" class="btn btn-primary" id="btnSimpan" disabled>Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        <div class="row mt-5 p-5">
            <div class="card mx-auto">
                <div class="card-body" style="padding: 20px; margin: 20px;">
                    <div class="table-responsive">
                        <table id="dataPetugas" class="display">
                            <thead>
                                <tr>
                                    <th>Id Kredit</th>
                                    <th>Id Anggota</th>
                                    <th>Id Angsuran</th>
                                    <th>Nama Anggota</th>
                                    <th>Jumlah Pinjaman</th>
                                    <th>Administrasi</th>
                                    <th>Nominal Pembayaran</th>
                                    <th>Terlambat</th>
                                    <th>Denda</th>
                                    <th>Tanggal Bayar</th>
                                    <th>Tanggal Tempo</th>
                                    <th>Total Pembayaran</th>
                                    <th>Status</th>
                                    <th>Tenor Bulan</th>
                                    <th>Sisa Angsuran</th>
                                    <th>Keterangan</th>
                                    <th>Bukti Transaksi</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Data will be populated by DataTables -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->
    <!-- DataTables JS -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <!-- Sweetalert --> 
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- jQuery Mask Plugin -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <!-- Custom JS -->
     <script>
        let session = '<?= $_SESSION['role']?>'
        console.log(session);
     </script>
    <script src="../assets/js/angsuran-anggota.js"></script>
</body>
</html>
