<?php
session_start();
// include('../koneksi/koneksi.php');
if (!isset($_SESSION['login_user'])) {
    header("Location: ../index.php");
    exit(); 
}
if (!isset($_GET['kredit']) AND !isset($_GET['anggota'])) {
    header("Location: laporan.php");
    exit(); 
} else {
    $id_kredit = $_GET['kredit'];
    $id_anggota = $_GET['anggota'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Petugas</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- DataTables CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css">
    <!-- Sweetalert -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="../assets/css/pinjam-pages.css">
</head>
<body>
<?php require_once('template/navigation.php') ?>
<div class="container-fluid">
    <div class="row mt-5 p-5">
            <div class="card mx-auto">
            <div class="card-header">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <span>Angsuran</span>
                            </div>
                            <div class="col">
                                <a class="btn btn-primary float-right" href="export-pinjaman.php?export=angsuran&id_kredit=<?= $id_kredit ?>" role="button">Print</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body" style="padding: 20px; margin: 20px;">
                    <div class="table-responsive">
                        <table id="dataAngsuran" class="display">
                            <thead>
                                <tr>
                                    <th>Id Kredit</th>
                                    <th>Id Anggota</th>
                                    <th>Nama Anggota</th>
                                    <th>Jumlah Pinjaman</th>
                                    <th>Administrasi</th>
                                    <th>Nominal Pembayaran</th>
                                    <th>Terlambat</th>
                                    <th>Denda</th>
                                    <th>Tanggal Bayar</th>
                                    <th>Total Pembayaran</th>
                                    <th>Status</th>
                                    <th>Tenor Bulan</th>
                                    <th>Sisa Angsuran</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Data will be populated by DataTables -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- End  -->
    </div>
</div>
</div>
<script>
    let id_kredit = "<?= $id_kredit ?>";
    let id_anggota = "<?= $id_anggota ?>";
</script>

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->
    <!-- DataTables JS -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <!-- Sweetalert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- jQuery Mask Plugin -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <!-- Custom JS -->
    <script src="../assets/js/laporan-angsuran.js"></script>
</body>
</html>
