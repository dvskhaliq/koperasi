<?php
session_start();
require_once '../app/koneksi/koneksi.php';
require_once '../app/lib/fpdf/fpdf.php';
// require_once '../app/controller/fetch-export.php';

class PDF extends FPDF
{
    // Page header
    function Header()
    {
        // Logo
        $this->Image('../assets/image/logo.jpeg',10,6,30);
        // Arial bold 15
        $this->SetFont('Arial','B',15);
        // Move to the right
        $this->Cell(35);
        // Title
        $this->Cell(70,30,'Laporan Simpanan Anggota',0,1,'C');
        // Line break
        $this->Ln(20);
    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }

    // Table header
    function TableHeader($headerWidth)
    {
        $this->SetX(($this->w - $headerWidth) / 2);
        $this->SetFont('Arial', 'B', 12);
        $this->Cell(30, 10, 'ID Anggota', 1, 0, 'C');
        $this->Cell(40, 10, 'ID Transaksi', 1, 0, 'C');
        $this->Cell(60, 10, 'Nama Anggota', 1, 0, 'C');
        $this->Cell(40, 10, 'Nominal Simpanan', 1, 0, 'C');
        $this->Cell(40, 10, 'Tanggal Setoran', 1, 0, 'C');
        $this->Cell(40, 10, 'Jenis Simpanan', 1, 0, 'C');
        $this->Ln();
    }

    // Table rows
    function TableRow($data, $rowWidth)
    {
        $this->SetFont('Arial', '', 12);
        foreach ($data as $item) {
            $this->SetX(($this->w - $rowWidth) / 2);
            $this->Cell(30, 10, $item['id_anggota'], 1, 0, 'C');
            $this->Cell(40, 10, $item['id_transaksi'], 1, 0, 'C');
            $this->Cell(60, 10, $item['nama_anggota'], 1, 0, 'C');
            $this->Cell(40, 10, 'Rp ' . number_format($item['nominal_simpanan'], 2, ',', '.'), 1, 0, 'C');
            $this->Cell(40, 10, $item['tanggal'], 1, 0, 'C');
            $this->Cell(40, 10, $item['jenis_simpanan'], 1, 0, 'C');
            $this->Ln();
        }
    }
    function Signed() {
        $this->Cell(450,30,'Hormat Kami',0,1,'C');
        $this->Ln();
        $this->Cell(450,0,'( Koperasi Bung Karto )',0,1,'C');
    }
}

// Instanciation of inherited class
if (isset($_GET['export']) && $_GET['export'] === 'simpanan') {
    if (isset($_SESSION['login_user']) AND $_SESSION['role'] === "Admin") {
        if (isset($_GET['start_date']) AND isset($_GET['end_date'])) {
            $start_date = $_GET['start_date'];
            $end_date = $_GET['end_date'];
            $sql = "SELECT info.id_anggota, simpan.id_transaksi, info.nama_anggota, simpan.nominal_simpanan, simpan.tanggal, simpan.jenis_simpanan 
                    FROM info_anggota AS info 
                    INNER JOIN simpanan_anggota AS simpan ON info.id_anggota = simpan.id_anggota
                    WHERE simpan.tanggal >= '$start_date' AND simpan.tanggal <= '$end_date'";
            $result = $conn->query($sql);
        } else {
            $sql = "SELECT info.id_anggota, simpan.id_transaksi, info.nama_anggota, simpan.nominal_simpanan, simpan.tanggal, simpan.jenis_simpanan 
                    FROM info_anggota AS info 
                    INNER JOIN simpanan_anggota AS simpan ON info.id_anggota = simpan.id_anggota";
            $result = $conn->query($sql);
        }
    } elseif (isset($_SESSION['login_user']) AND $_SESSION['role'] === "Users") {
        $user_id = $_SESSION['login_user'];
        if (isset($_GET['start_date']) AND isset($_GET['end_date'])) {
            $start_date = $_GET['start_date'];
            $end_date = $_GET['end_date'];
            $sql = "SELECT info.id_anggota, simpan.id_transaksi, info.nama_anggota, simpan.nominal_simpanan, simpan.tanggal, simpan.jenis_simpanan 
                    FROM info_anggota AS info 
                    INNER JOIN simpanan_anggota AS simpan ON info.id_anggota = simpan.id_anggota
                    WHERE simpan.tanggal >= '$start_date' AND simpan.tanggal <= '$end_date' AND info.id_anggota='$user_id'";
            $result = $conn->query($sql);
        } else {
            $sql = "SELECT info.id_anggota, simpan.id_transaksi, info.nama_anggota, simpan.nominal_simpanan, simpan.tanggal, simpan.jenis_simpanan 
                    FROM info_anggota AS info 
                    INNER JOIN simpanan_anggota AS simpan ON info.id_anggota = simpan.id_anggota 
                    WHERE info.id_anggota='$user_id'";
            $result = $conn->query($sql);
        }
    }

    $data = array();

    while($row = $result->fetch_assoc()) {
        $data[] = $row;
    }

    $pdf = new PDF('L', 'mm', 'A4');
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $headerWidth = 30 + 40 + 60 + 40 + 40 + 40; // Total width of the header
    $rowWidth = $headerWidth; // Row width is the same as header width
    $pdf->TableHeader($headerWidth);
    $pdf->TableRow($data, $rowWidth);
    $pdf->Signed();
    $pdf->Output();
} else {
    header('Location: laporan.php');
}
?>
