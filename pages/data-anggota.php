<?php
session_start();
if (!isset($_SESSION['login_user']) OR $_SESSION['role'] !== 'Admin') {
    header("Location: ../index.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Petugas</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- DataTables CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css">
    <!-- Sweetalert -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">
    <!-- Custom CSS -->
    <!-- <link href="../assets/css/home.css" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="../assets/css/pinjam-pages.css">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="home.php">
                <img src="../assets/image/logo.jpeg" width="50" height="50" alt="">
            </a>
            <div class="collapse navbar-collapse" id="navbarNav">
            <?php if (isset($_SESSION['role']) AND $_SESSION['role'] === 'Admin') {?>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="home.php">
                            Home
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                            Anggota
                        </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="home.php">Akses Anggota</a>
                                <a class="dropdown-item" href="data-anggota.php">Data Anggota</a>
                            </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                            Keuangan
                        </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="pinjaman-anggota.php">Pinjaman Anggota</a>
                                <a class="dropdown-item" href="simpanan-anggota.php">Simpanan Anggota</a>
                                <a class="dropdown-item" href="pengambilan-saldo.php">Pengambilan Dana</a>
                            </div>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="laporan.php">Laporan</a></li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a class="nav-link" href="logout.php">Log Out</a></li>
                </ul>
                <?php } else if(isset($_SESSION['role']) AND $_SESSION['role'] === 'Users') {?>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="home.php">
                                Home
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                                Keuangan
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="form-pinjaman-anggota.php">Pengajuan Pinjaman</a>
                                <a class="dropdown-item" href="simpanan-anggota.php">Simpanan Anggota</a>
                                <a class="dropdown-item" href="pengambilan-saldo.php">Pengambilan Dana</a>
                            </div>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="laporan.php">Laporan</a></li>
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item"><a class="nav-link" href="logout.php">Log Out</a></li>
                    </ul>
                <?php }?>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="content mt-5">
                <div class="row">
                    <div class="card mx-auto" style="width: 50rem;">
                        <div class="card-body" style="padding: 20px; margin: 20px;">
                            <form id="petugasForm">
                                <div class="form-group">
                                    <input type="hidden" id="action" name="action" value="add">
                                </div>
                                <div class="form-group">
                                    <label for="id_anggota">Username Anggota</label>
                                    <select class="form-control" id="id_anggota" name="id_anggota" required>
                                        <!-- Options akan diisi menggunakan jQuery -->
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="jabatan">Nama Anggota</label>
                                    <input type="text" class="form-control" id="namaAnggota" name="namaAnggota">
                                </div>
                                <div class="form-group">
                                    <label for="jabatan">Kota Kelahiran</label>
                                    <input type="text" class="form-control" id="kotaLahir" name="kotaLahir">
                                </div>
                                <div class="form-group">
                                    <label for="tanggal">Tanggal Lahir</label>
                                    <input type="date" class="form-control" id="tglLahir" name="tglLahir">
                                </div> 
                                <div class="form-group">
                                    <label for="username">Telphone</label>
                                    <input type="text" class="form-control" id="tlp" name="tlp">
                                </div>
                                <div class="form-group">
                                    <label for="username">Alamat</label>
                                    <textarea type="text" class="form-control" id="address" name="address"></textarea>
                                </div>
                                <button type="button" class="btn btn-danger" id="btnBatal">Batal</button>
                                <button type="button" class="btn btn-primary" id="btnSimpan">Simpan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5 p-5">
                <div class="card">
                    <div class="card-body">
                        <table id="dataPetugas" class="display">
                            <thead>
                                <tr>
                                    <th>Id Anggota</th>
                                    <th>Nama Anggota</th>
                                    <th>Telphone</th>
                                    <th>Kota Lahir</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Alamat</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Data will be populated by DataTables -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->
    <!-- DataTables JS -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <!-- Sweetalert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- Custom JS -->
    <script src="../assets/js/data-anggota.js"></script>
</body>
</html>
