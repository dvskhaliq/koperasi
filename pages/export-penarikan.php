<?php
session_start();
require_once '../app/koneksi/koneksi.php';
require_once '../app/lib/fpdf/fpdf.php';
// require_once '../app/controller/fetch-export.php';

class PDF extends FPDF
{
    // Page header
    function Header()
    {
        // Logo
        $this->Image('../assets/image/logo.jpeg',10,6,30);
        // Arial bold 15
        $this->SetFont('Arial','B',15);
        // Move to the right
        $this->Cell(45);
        // Title
        $this->Cell(60,30,'Laporan Penarikan Saldo Anggota',0,1,'C');
        // Line break
        $this->Ln(20);
    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
        
    }

    // Table header
    function TableHeader($headerWidth)
    {
        $this->SetX(($this->w - $headerWidth) / 2);
        $this->SetFont('Arial', 'B', 12);
        $this->Cell(30, 10, 'ID Anggota', 1, 0, 'C');
        $this->Cell(40, 10, 'ID Transaksi', 1, 0, 'C');
        $this->Cell(60, 10, 'Nama Anggota', 1, 0, 'C');
        $this->Cell(40, 10, 'Tanggal Penarikan', 1, 0, 'C');
        $this->Cell(40, 10, 'Nominal Simpanan', 1, 0, 'C');
        $this->Cell(40, 10, 'Simpanan Sukarela', 1, 0, 'C');
        $this->Ln();
    }

    // Table rows
    function TableRow($data, $rowWidth)
    {
        $this->SetFont('Arial', '', 12);
        foreach ($data as $item) {
            $this->SetX(($this->w - $rowWidth) / 2);
            $this->Cell(30, 10, $item['id_anggota'], 1, 0, 'C');
            $this->Cell(40, 10, $item['id_transaksi'], 1, 0, 'C');
            $this->Cell(60, 10, $item['nama_anggota'], 1, 0, 'C');
            $this->Cell(40, 10, $item['tanggal'], 1, 0, 'C');
            $this->Cell(40, 10, 'Rp ' . number_format($item['simpanan_sukarela'], 2, ',', '.'), 1, 0, 'C');
            $this->Cell(40, 10, $item['simpanan_sukarela'], 1, 0, 'C');
            $this->Ln();
        }
    }
    function Signed() {
        $this->Cell(450,30,'Hormat Kami',0,1,'C');
        $this->Ln();
        $this->Cell(450,0,'( Koperasi Bung Karto )',0,1,'C');
    }
}

// Instanciation of inherited class
if (isset($_GET['export']) && $_GET['export'] === 'penarikan') {
    if (isset($_SESSION['login_user']) AND $_SESSION['role'] === "Admin") {
        if (isset($_GET['start_date']) AND isset($_GET['end_date'])) {
            $start_date = $_GET['start_date'];
            $end_date = $_GET['end_date'];
            $sql = "SELECT penarikan.id_transaksi, penarikan.id_anggota, info.nama_anggota, penarikan.tanggal, penarikan.simpanan_sukarela, penarikan.jumlah_penarikan 
                    FROM penarikan_saldo_anggota AS penarikan 
                    INNER JOIN info_anggota AS info 
                    ON penarikan.id_anggota=info.id_anggota 
                    INNER JOIN simpanan_anggota AS simpan 
                    ON penarikan.id_anggota = simpan.id_anggota AND simpan.jenis_simpanan='Simpanan Sukarela'
                    WHERE penarikan.tanggal >= '$start_date' AND penarikan.tanggal <= '$end_date'";
            $result = $conn->query($sql);
        } else {
            $sql = "SELECT penarikan.id_transaksi, penarikan.id_anggota, info.nama_anggota, penarikan.tanggal, penarikan.simpanan_sukarela, penarikan.jumlah_penarikan 
                    FROM penarikan_saldo_anggota AS penarikan 
                    INNER JOIN info_anggota AS info 
                    ON penarikan.id_anggota=info.id_anggota 
                    INNER JOIN simpanan_anggota AS simpan 
                    ON penarikan.id_anggota = simpan.id_anggota AND simpan.jenis_simpanan='Simpanan Sukarela'";
            $result = $conn->query($sql);
        }
    } elseif (isset($_SESSION['login_user']) AND $_SESSION['role'] === "Users") {
        $user_id = $_SESSION['login_user'];
        if (isset($_GET['start_date']) AND isset($_GET['end_date'])) {
            $start_date = $_GET['start_date'];
            $end_date = $_GET['end_date'];
            $sql = "SELECT penarikan.id_transaksi, penarikan.id_anggota, info.nama_anggota, penarikan.tanggal, penarikan.simpanan_sukarela, penarikan.jumlah_penarikan 
                    FROM penarikan_saldo_anggota AS penarikan 
                    INNER JOIN info_anggota AS info ON penarikan.id_anggota=info.id_anggota 
                    INNER JOIN simpanan_anggota AS simpan ON penarikan.id_anggota = simpan.id_anggota AND simpan.jenis_simpanan='Simpanan Sukarela' 
                    WHERE penarikan.id_anggota='$user_id' AND penarikan.tanggal >= '$start_date' AND penarikan.tanggal <= '$end_date'";
            $result = $conn->query($sql);
        }
        $sql = "SELECT penarikan.id_transaksi, penarikan.id_anggota, info.nama_anggota, penarikan.tanggal, penarikan.simpanan_sukarela, penarikan.jumlah_penarikan 
                FROM penarikan_saldo_anggota AS penarikan 
                INNER JOIN info_anggota AS info ON penarikan.id_anggota=info.id_anggota 
                INNER JOIN simpanan_anggota AS simpan ON penarikan.id_anggota = simpan.id_anggota AND simpan.jenis_simpanan='Simpanan Sukarela' WHERE penarikan.id_anggota='$user_id'";
        $result = $conn->query($sql);
    }

    $data = array();

    while($row = $result->fetch_assoc()) {
        $data[] = $row;
    }

    $pdf = new PDF('L', 'mm', 'A4');
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $headerWidth = 30 + 40 + 60 + 40 + 40 + 40; // Total width of the header
    $rowWidth = $headerWidth; // Row width is the same as header width
    $pdf->TableHeader($headerWidth);
    $pdf->TableRow($data, $rowWidth);
    $pdf->Signed();
    $pdf->Output();
} else {
    header('Location: laporan.php');
}
?>
