<?php
session_start();
if (!isset($_SESSION['login_user'])) {
    header("Location: ../index.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Petugas</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- DataTables CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css">
    <!-- Sweetalert -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">
    <!-- Custom CSS -->
    <link href="../assets/css/pinjam-pages.css" rel="stylesheet">
</head>
<body>
<?php require_once('template/navigation.php') ?>
    <div class="container-fluid">
        <div class="content mt-5">
            <div class="row">
                <div class="card mx-auto" style="width: 50rem;">
                    <div class="card-body" style="padding: 20px; margin: 20px;">
                    <form id="petugasForm" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="hidden" id="action" name="action" value="add">
                    </div>
                    <div class="form-group">
                        <label for="id_anggota">Id Transaksi</label>
                        <input type="text" class="form-control" id="id_transaksi" name="id_transaksi" readonly>
                    </div>
                    <div class="form-group">
                        <label for="jabatan">Nama Lengkap Anggota</label>
                        <select class="form-control" id="id_anggota" name="id_anggota" required>
                            <!-- Options akan diisi menggunakan jQuery -->
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nominal">Nominal Simpanan</label>
                        <input type="text" class="form-control" id="nominal" name="nominal">
                    </div>
                    <div class="form-group">
                        <label for="tanggal">tanggal</label>
                        <input type="date" class="form-control" id="tgl" name="tgl">
                    </div>
                    <div class="form-group">
                        <label for="tanggal">Bukti Transaksi</label>
                        <div class="input-group mb-3">
                            <input type="file" name="file" id="file" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jenis">Jenis Simpanan</label>
                        <select class="form-control" id="jenis" name="jenis" required>
                            <option value="Simpanan Wajib">Simpanan Wajib</option>
                            <option value="Simpanan Sukarela">Simpanan Sukarela</option>
                        </select>
                    </div>
                    <?php if (isset($_SESSION['role']) AND $_SESSION['role'] === 'Admin') {?>
                    <div class="form-group">
                        <label for="jenis">Status</label>
                        <select class="form-control" id="status" name="status" required>
                            <option>Silahkan pilih status</option>
                            <option value="pending">Pending</option>
                            <option value="approved">Approved</option>
                            <option value="rejected">Rejected</option>
                        </select>
                    </div>
                    <?php } ?>
                    <button type="button" class="btn btn-danger" id="btnBatal">Batal</button>
                    <button type="button" class="btn btn-primary" id="btnSimpan">Simpan</button>
                </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5 p-3">
            <div class="card mx-auto">
                    <div class="card-body" style="padding: 20px; margin: 20px;">
                        <table id="dataPetugas" class="display">
                            <thead>
                                <tr>
                                    <th>Id Transaksi</th>
                                    <th>Id Anggota</th>
                                    <th>Nama Anggota</th>
                                    <th>Nominal Simpanan</th>
                                    <th>Tanggal</th>
                                    <th>Jenis Simpanan</th>
                                    <th>Status</th>
                                    <th>Bukti Transaksi</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Data will be populated by DataTables -->
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->
    <!-- DataTables JS -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <!-- Sweetalert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- jQuery Mask Plugin -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <!-- Custom JS -->
    <script src="../assets/js/simpanan-anggota.js"></script>
</body>
</html>
