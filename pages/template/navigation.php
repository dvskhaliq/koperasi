<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="home.php">
        <img src="../assets/image/logo.jpeg" width="50" height="50" alt="">
    </a>
        <div class="collapse navbar-collapse" id="navbarNav">
            <?php if (isset($_SESSION['role']) AND $_SESSION['role'] === 'Admin') {?>
                <ul class="navbar-nav">
                    <li class="nav-item">
                            <a class="nav-link" href="home.php">
                                Home
                            </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                            Anggota
                        </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="home.php">Akses Anggota</a>
                                <a class="dropdown-item" href="data-anggota.php">Data Anggota</a>
                            </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                            Keuangan
                        </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="pinjaman-anggota.php">Pinjaman Anggota</a>
                                <a class="dropdown-item" href="simpanan-anggota.php">Simpanan Anggota</a>
                                <a class="dropdown-item" href="pengambilan-saldo.php">Pengambilan Dana</a>
                            </div>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="laporan.php">Laporan</a></li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a class="nav-link" href="logout.php">Log Out</a></li>
                </ul>
                <?php } else if(isset($_SESSION['role']) AND $_SESSION['role'] === 'Users') {?>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="home.php">
                                Home
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                                Keuangan
                            </a> 
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="pinjaman-anggota.php">Pinjaman Anggota</a>
                                <a class="dropdown-item" href="form-pinjaman-anggota.php">Pengajuan Pinjaman</a>
                                <a class="dropdown-item" href="simpanan-anggota.php">Simpanan Anggota</a>
                                <a class="dropdown-item" href="pengambilan-saldo.php">Pengambilan Dana</a>
                            </div>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="laporan.php">Laporan</a></li>
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item"><a class="nav-link" href="logout.php">Log Out</a></li>
                    </ul>
                <?php }?>
            </div>
        </nav>