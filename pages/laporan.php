<?php
session_start();
// include('../koneksi/koneksi.php');
if (!isset($_SESSION['login_user'])) {
    header("Location: ../index.php");
    exit(); 
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Petugas</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- DataTables CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css">
    <!-- Sweetalert -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="../assets/css/pinjam-pages.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
</head>
<body>
<?php require_once('template/navigation.php') ?>
    <div class="container-fluid">
        <div class="row mt-5 p-5">
            <div class="card mx-auto">
                <div class="card-header">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <span>Simpanan Anggota</span>
                            </div>
                            <div class="col">
                                <a class="btn btn-primary float-right" id="print-report-simpanan" href="export-simpanan.php?export=simpanan" role="button">Print</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" id="filterdate-simpanan" name="filterdate-simpanan" placeholder="Select Date Range" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body" style="padding: 20px; margin: 20px;">
                    <div class="table-responsive">
                    <table id="dataSimpanan" class="display">
                            <thead>
                                <tr>
                                    <th>Id Transaksi</th>
                                    <th>Id Anggota</th>
                                    <th>Nama Anggota</th>
                                    <th>Nominal Simpanan</th>
                                    <th>Tanggal</th>
                                    <th>Jenis Simpanan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Data will be populated by DataTables -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5 p-5">
            <div class="card mx-auto">
            <div class="card-header">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <span>Pengambilan Saldo Anggota</span>
                            </div>
                            <div class="col">
                                <a class="btn btn-primary float-right" id="print-report-saldo" href="export-penarikan.php?export=penarikan" role="button">Print</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" id="filterdate-penarikan" name="filterdate-penarikan" placeholder="Select Date Range" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body" style="padding: 20px; margin: 20px;">
                    <div class="table-responsive">
                        <table id="dataSaldo" class="display">
                            <thead>
                                <tr>
                                    <th>Id Transaksi</th>
                                    <th>Id Anggota</th>
                                    <th>Nama Anggota</th>
                                    <th>Tanggal</th>
                                    <th>Simpanan Sukarela</th>
                                    <th>Jumlah Penarikan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Data will be populated by DataTables -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="row mt-5 p-5">
            <div class="card mx-auto">
            <div class="card-header">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <span>Pinjaman Anggota</span>
                            </div>
                            <div class="col">
                                <a class="btn btn-primary float-right" id="print-report-pinjaman" href="export-pinjaman.php?export=pinjaman" role="button">Print</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="text" id="filterdate-pinjaman" name="filterdate-pinjaman" placeholder="Select Date Range" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body" style="padding: 20px; margin: 20px;">
                    <div class="table-responsive">
                        <table id="dataPinjaman" class="display">
                            <thead>
                                <tr>
                                    <th>Id Kredit</th>
                                    <th>Nama Anggota</th>
                                    <th>Tanggal</th>
                                    <th>Saldo</th>
                                    <th>Jumlah Pinjaman</th>
                                    <th>Administrasi</th>
                                    <th>Lama Cicilan</th>
                                    <th>Angsuran Perbulan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Data will be populated by DataTables -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- End  -->
    </div>
</div>
</div>

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->
    <!-- DataTables JS -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <!-- Sweetalert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- jQuery Plugin -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script> -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <!-- Custom JS -->
    <script src="../assets/js/laporan.js"></script>
</body>
</html>
