<?php

session_start();
// Cek apakah parameter file ada dalam URL
if(isset($_SESSION['login_user']) AND isset($_GET['file'])) {
    $file = $_GET['file'];
    
    // Tentukan path lengkap dari file yang akan diunduh
    $filepath = '../assets/image/uploads/' . $file;

    // Periksa apakah file tersebut ada
    if(file_exists($filepath)) {
        // Tetapkan header yang sesuai
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($filepath));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filepath));
        
        // Baca file dan kirim ke output
        readfile($filepath);
        exit;
    } else {
        // Jika file tidak ditemukan, tampilkan pesan error
        echo "File not found.";
    }
} else {
    // Jika parameter file tidak ada, tampilkan pesan error
    header("Location: simpanan-anggota.php");
    exit();
}
?>
