-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 31 Agu 2024 pada 08.09
-- Versi server: 10.4.32-MariaDB
-- Versi PHP: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `koperasi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggota`
--

CREATE TABLE `anggota` (
  `id_anggota` varchar(100) NOT NULL,
  `jabatan` varchar(20) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `anggota`
--

INSERT INTO `anggota` (`id_anggota`, `jabatan`, `username`, `password`) VALUES
('AGT1098376', 'Developer', 'dvskhaliq', 'P@ssw0rd'),
('AGTor7igo1r4lp', 'CS', 'RANI', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `angsuran_anggota`
--

CREATE TABLE `angsuran_anggota` (
  `id_angsuran` varchar(100) NOT NULL,
  `id_kredit` varchar(100) DEFAULT NULL,
  `id_anggota` varchar(100) DEFAULT NULL,
  `nominal_pembayaran` char(16) NOT NULL,
  `tgl_kredit` date DEFAULT NULL,
  `tanggal_bayar` date NOT NULL,
  `denda` char(16) NOT NULL,
  `total_pembayaran` char(16) NOT NULL,
  `terlambat` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `tenor_bulan` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `bukti_transaksi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `angsuran_anggota`
--

INSERT INTO `angsuran_anggota` (`id_angsuran`, `id_kredit`, `id_anggota`, `nominal_pembayaran`, `tgl_kredit`, `tanggal_bayar`, `denda`, `total_pembayaran`, `terlambat`, `status`, `tenor_bulan`, `keterangan`, `bukti_transaksi`) VALUES
('ANGS0h7xa6xv8c38', 'KRDTg6z8770cyg6', 'AGT1098376', '375950', '2024-09-30', '2024-10-01', '18798', '394748', 1, 'approved', 1, 'Sukses sudah di bayar', ''),
('ANGS21cju5gvdv4', 'KRDTg6z8770cyg6', 'AGT1098376', '375950', '2024-09-30', '2024-10-01', '18798', '394748', 1, 'pending', 1, 'TEST', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `info_anggota`
--

CREATE TABLE `info_anggota` (
  `id_anggota` varchar(100) DEFAULT NULL,
  `nama_anggota` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `kota_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `telpon` char(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `info_anggota`
--

INSERT INTO `info_anggota` (`id_anggota`, `nama_anggota`, `alamat`, `kota_lahir`, `tanggal_lahir`, `telpon`) VALUES
('AGT1098376', 'DAVIS CHALIQ', 'Jalan mulu jadian kagak', 'Tangerang', '1999-11-10', '081284537864'),
('AGTor7igo1r4lp', 'SUCI RANI', 'Masuk Pak Haji', 'TANGERANG', '2024-08-21', '081284537864');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penarikan_saldo_anggota`
--

CREATE TABLE `penarikan_saldo_anggota` (
  `id_transaksi` varchar(100) NOT NULL,
  `id_anggota` varchar(100) NOT NULL,
  `simpanan_sukarela` char(16) NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah_penarikan` char(16) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `penarikan_saldo_anggota`
--

INSERT INTO `penarikan_saldo_anggota` (`id_transaksi`, `id_anggota`, `simpanan_sukarela`, `tanggal`, `jumlah_penarikan`, `status`) VALUES
('TRKjs045khmjk', 'AGT1098376', '10000000', '2024-07-22', '1000000', 'approved'),
('TRKlh2v446xluo', 'AGT1098376', '9000000', '2024-07-22', '5000000', 'approved');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pinjaman_anggota`
--

CREATE TABLE `pinjaman_anggota` (
  `id_kredit` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `id_anggota` varchar(100) NOT NULL,
  `nik` char(20) NOT NULL,
  `telpon` varchar(16) NOT NULL,
  `kota_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `alamat` text NOT NULL,
  `saldo` char(20) NOT NULL,
  `jumlah_pinjaman` char(20) NOT NULL,
  `administrasi` char(20) NOT NULL,
  `lama_cicilan` int(5) NOT NULL,
  `angsuran_perbulan` char(20) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `pinjaman_anggota`
--

INSERT INTO `pinjaman_anggota` (`id_kredit`, `tanggal`, `id_anggota`, `nik`, `telpon`, `kota_lahir`, `tanggal_lahir`, `alamat`, `saldo`, `jumlah_pinjaman`, `administrasi`, `lama_cicilan`, `angsuran_perbulan`, `status`) VALUES
('KRDTg6z8770cyg6', '2024-08-31', 'AGT1098376', '129084901824091', '081284537864', 'Tangerang', '1999-11-10', 'Jalan mulu jadian kagak', '365000', '365000', '10950', 1, '375950', 'approved'),
('KRDTn7nyi1t4m', '2024-07-27', 'AGT1098376', '12984912', '081284537864', 'Tangerang', '1999-11-11', 'Jalan mulu jadian kagak', '4485000', '4000000', '120000', 3, '1373334', 'approved'),
('KRDTxtllag09fxn', '2024-07-24', 'AGT1098376', '367190128490214', '081284537864', 'Tangerang', '1999-11-11', 'Jalan mulu jadian kagak', '5000000', '1000000', '30000', 2, '515000', 'approved');

-- --------------------------------------------------------

--
-- Struktur dari tabel `simpanan_anggota`
--

CREATE TABLE `simpanan_anggota` (
  `id_transaksi` varchar(100) NOT NULL,
  `id_anggota` varchar(100) DEFAULT NULL,
  `nominal_simpanan` char(100) NOT NULL,
  `tanggal` date NOT NULL,
  `jenis_simpanan` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `bukti_transaksi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `simpanan_anggota`
--

INSERT INTO `simpanan_anggota` (`id_transaksi`, `id_anggota`, `nominal_simpanan`, `tanggal`, `jenis_simpanan`, `status`, `bukti_transaksi`) VALUES
('TRX0msbnk6ejet', 'AGT1098376', '10000000', '2024-07-21', 'Simpanan Sukarela', 'approved', 'image (22).png'),
('TRXw47ot2eycdj', 'AGT1098376', '5000000', '2024-07-21', 'Simpanan Wajib', 'approved', 'image (21).png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES
(1, 'admin', 'P@ssw0rd', 'Admin');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indeks untuk tabel `angsuran_anggota`
--
ALTER TABLE `angsuran_anggota`
  ADD PRIMARY KEY (`id_angsuran`),
  ADD KEY `id_kredit` (`id_kredit`),
  ADD KEY `id_anggota` (`id_anggota`);

--
-- Indeks untuk tabel `info_anggota`
--
ALTER TABLE `info_anggota`
  ADD KEY `id_anggota` (`id_anggota`);

--
-- Indeks untuk tabel `penarikan_saldo_anggota`
--
ALTER TABLE `penarikan_saldo_anggota`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_anggota` (`id_anggota`);

--
-- Indeks untuk tabel `pinjaman_anggota`
--
ALTER TABLE `pinjaman_anggota`
  ADD PRIMARY KEY (`id_kredit`),
  ADD KEY `id_anggota` (`id_anggota`);

--
-- Indeks untuk tabel `simpanan_anggota`
--
ALTER TABLE `simpanan_anggota`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_anggota` (`id_anggota`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `angsuran_anggota`
--
ALTER TABLE `angsuran_anggota`
  ADD CONSTRAINT `angsuran_anggota_ibfk_1` FOREIGN KEY (`id_kredit`) REFERENCES `pinjaman_anggota` (`id_kredit`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `angsuran_anggota_ibfk_2` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id_anggota`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `info_anggota`
--
ALTER TABLE `info_anggota`
  ADD CONSTRAINT `info_anggota_ibfk_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id_anggota`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `penarikan_saldo_anggota`
--
ALTER TABLE `penarikan_saldo_anggota`
  ADD CONSTRAINT `penarikan_saldo_anggota_ibfk_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id_anggota`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pinjaman_anggota`
--
ALTER TABLE `pinjaman_anggota`
  ADD CONSTRAINT `pinjaman_anggota_ibfk_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id_anggota`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `simpanan_anggota`
--
ALTER TABLE `simpanan_anggota`
  ADD CONSTRAINT `simpanan_anggota_ibfk_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id_anggota`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
