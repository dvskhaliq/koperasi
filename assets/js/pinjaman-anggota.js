$(document).ready(function() {

    let table;
    var id = "KRDT" + Math.random().toString(36).slice(2);
    $('#id_kredit').val(id);
    $('#action').val('post');
    $('#nama-anggota').show();
    $('#field-idanggota').hide();

    $.ajax({
        url: 'http://localhost:1199/koperasi/app/controller/fetch-session.php',
        method: 'GET',
        success: function(response) {
            var result = JSON.parse(response);
            if (result.loggedIn && result.role === 'Admin') {
                table = $('#dataPetugas').DataTable(  {
                    "ajax": "http://localhost:1199/koperasi/app/controller/fetch-kredit-anggota.php",
                    "columns":[
                        { "data": "id_kredit" },
                        { "data": "id_anggota" }, 
                        { "data": "nama_anggota" },
                        {"data": "nik"},
                        {"data": "telpon"},
                        {"data": "kota_lahir"},
                        {"data": "tanggal_lahir"},
                        {"data": "alamat"},
                        { "data": "tanggal" },
                        { "data": "saldo",
                          "render": function(data, type, row) {
                                         return new Intl.NumberFormat('id-ID', {
                                            style: 'currency',
                                            currency: 'IDR'
                                        }).format(data);
                                    }
                        },
                        { "data": "jumlah_pinjaman",
                          "render": function(data, type, row) {
                                         return new Intl.NumberFormat('id-ID', {
                                            style: 'currency',
                                            currency: 'IDR'
                                        }).format(data);
                                    }
                        },
                        { "data": "administrasi",
                          "render": function(data, type, row) {
                                         return new Intl.NumberFormat('id-ID', {
                                            style: 'currency',
                                            currency: 'IDR'
                                        }).format(data);
                                    }
                        },
                        { "data": "lama_cicilan" },
                        { "data": "angsuran_perbulan",
                          "render": function(data, type, row) {
                                         return new Intl.NumberFormat('id-ID', {
                                            style: 'currency',
                                            currency: 'IDR'
                                        }).format(data);
                                    }
                        },
                        {"data": "status"},
                        { 
                            "data": null, 
                            "render": function(data, type, row) {
                                    return `<a class='btn btn-primary btn-sm btnAngsuran' href='detail-angsuran.php?kredit=${row.id_kredit}&anggota=${row.id_anggota}'>Angsuran</a>
                                            <button class='btn btn-success btn-sm editBtn'>Edit</button>
                                            <button class='btn btn-danger btn-sm btnDelete'>Hapus</button>`;
                            }
                            // "defaultContent": "<a class='btn btn-danger btn-sm btnAngsuran' href='detail-angsuran.php?kredit=${row.id_kredit}'>Info Angsuran</a><button class='btn btn-success btn-sm editBtn'>Edit</button><button class='btn btn-danger btn-sm btnDelete'>Hapus</button>" 
                        }
                    ]
                });
            } else if(result.loggedIn && result.role === 'Users') {
                table = $('#dataPetugas').DataTable(  {
                    "ajax": "http://localhost:1199/koperasi/app/controller/fetch-kredit-anggota.php",
                    "columns":[
                        { "data": "id_kredit" },
                        { "data": "id_anggota" }, 
                        { "data": "nama_anggota" },
                        {"data": "nik"},
                        {"data": "telpon"},
                        {"data": "kota_lahir"},
                        {"data": "tanggal_lahir"},
                        {"data": "alamat"},
                        { "data": "tanggal" },
                        { "data": "saldo",
                          "render": function(data, type, row) {
                                         return new Intl.NumberFormat('id-ID', {
                                            style: 'currency',
                                            currency: 'IDR'
                                        }).format(data);
                                    }
                        },
                        { "data": "jumlah_pinjaman",
                          "render": function(data, type, row) {
                                         return new Intl.NumberFormat('id-ID', {
                                            style: 'currency',
                                            currency: 'IDR'
                                        }).format(data);
                                    }
                        },
                        { "data": "administrasi",
                          "render": function(data, type, row) {
                                         return new Intl.NumberFormat('id-ID', {
                                            style: 'currency',
                                            currency: 'IDR'
                                        }).format(data);
                                    }
                        },
                        { "data": "lama_cicilan" },
                        { "data": "angsuran_perbulan",
                          "render": function(data, type, row) {
                                         return new Intl.NumberFormat('id-ID', {
                                            style: 'currency',
                                            currency: 'IDR'
                                        }).format(data);
                                    }
                        },
                        {"data": "status"},
                        { 
                            "data": null, 
                            "render": function(data, type, row) {
                                    return `<a class='btn btn-primary btn-sm btnAngsuran' href='detail-angsuran.php?kredit=${row.id_kredit}&anggota=${row.id_anggota}'>Angsuran</a>`;
                            }
                            // "defaultContent": "<a class='btn btn-danger btn-sm btnAngsuran' href='detail-angsuran.php?kredit=${row.id_kredit}'>Info Angsuran</a><button class='btn btn-success btn-sm editBtn'>Edit</button><button class='btn btn-danger btn-sm btnDelete'>Hapus</button>" 
                        }
                    ]
                });
            }
        }
    })

    function loadAnggotaList() {
        $.ajax({
            url: 'http://localhost:1199/koperasi/app/controller/fetch-anggota.php',
            method: 'GET',
            success: function(data) {
                var anggotaList = JSON.parse(data);
                $('#name_anggota').empty();
                $('#name_anggota').append('<option value="null">Silahkan anda pilih akun anggota</option>');
                $.each(anggotaList, function(key, value) {
                    $('#name_anggota').append('<option value="'+ value.id_anggota +'">'+ value.nama_anggota +'</option>');
                });
            }
        });
    }
    function setBatal () {
        var action = $('#action').val();
        if (action === 'edit') {
            $('#petugasForm')[0].reset();
            $('#action').val('post');
            var id = "KRDT" + Math.random().toString(36).slice(2);
            $('#id_kredit').val(id);
            $('#nama-anggota').show();
            $('#field-idanggota').hide();
            loadAnggotaList();
        }else {
            $('#petugasForm')[0].reset();
            var id = "KRDT" + Math.random().toString(36).slice(2);
            $('#id_kredit').val(id);
            // $('#action').val('post');
        }
    }
    function moneyFormatForm(data) {
        var formattedVal = new Intl.NumberFormat('id-ID').format(data);
        return formattedVal;
    }
    function calculated() {
        var jumlah_pinjaman = $('#jumlah_pinjaman').val().replace(/\./g, '');
        var administrasi = jumlah_pinjaman * 0.03;
        var formattedValPinjaman = moneyFormatForm(jumlah_pinjaman);
        var formattedValAdmin = moneyFormatForm(administrasi);
        var response = {"jumlah_pinjaman": formattedValPinjaman, "administrasi": formattedValAdmin};
        return response;
    }

    $('#tenor').on('input', function() {
        var inputTenor = $(this).val();
        console.log({"inputTenor": inputTenor});
        var inputPinjaman = $('#jumlah_pinjaman').val().replace(/\./g, '');
        var admin = $('#admin').val().replace(/\./g, '');
        if (inputTenor !== 0 || inputTenor !== '') {
            if (inputPinjaman !== '' || inputPinjaman !== 0) {
                var totalPinjaman = (Number(inputPinjaman) + Number(admin));
                var cicilan = Math.ceil( totalPinjaman / parseInt(inputTenor));
                if (cicilan > 0) {
                    var formatted = moneyFormatForm(cicilan);
                } else {
                    var formatted = 0;
                }
                $('#angsuran').val(formatted);
            }
        }
    });
    $('#jumlah_pinjaman').on('input', function() {
        var inputVal = $(this).val();
        var numericVal = inputVal.replace(/\./g, '');
        var formattedVal = moneyFormatForm(numericVal);
        var check = calculated(formattedVal);
        $(this).val(formattedVal);
        $('#admin').val(check.administrasi);
    });
    $('#angsuran').on('input', function() {
        var inputVal = $(this).val();
        var numericVal = inputVal.replace(/\./g, '');
        var formattedVal = moneyFormatForm(numericVal);
        $(this).val(formattedVal);
    });
    $('#admin').on('input', function() {
        var inputVal = $(this).val();
        var numericVal = inputVal.replace(/\./g, '');
        var formattedVal = moneyFormatForm(numericVal);
        $(this).val(formattedVal);
    });
    loadAnggotaList();

    $('#name_anggota').change(function() {
        var anggotaId = $(this).val();
        console.log(anggotaId);
        if (anggotaId !== 'null') {
        $.ajax({
            url: 'http://localhost:1199/koperasi/app/controller/add-pinjaman-anggota.php', // URL endpoint untuk mendapatkan total transaksi
            method: 'GET', 
            data: { id_anggota: anggotaId, action: 'saldo' },
            success: function(response) {
                var data = JSON.parse(response);
                if (data.alamat !== null) {
                    var number_string = data.saldo.replace(/[^,\d]/g, '').toString();
                    var formated = moneyFormatForm(number_string);
                    $('#tlp').val(data.telpon);
                    $('#address').val(data.alamat);
                    $('#kotaLahir').val(data.kota_lahir);
                    $('#tglLahir').val(data.tanggal_lahir);
                    $('#saldo').val(formated);
                } else {
                    setBatal();
                    Swal.fire(
                        'Error!',
                        'Saldo Anda Tidak Cukup',
                        'error'
                    );
                }
            },
            error: function(xhr, status, error) {
                console.error({Error: err, Status:  status, xhr: xhr});
            }
      });
    }
    });
    $('#dataPetugas tbody').on('click', '.editBtn', function () {
        var data = table.row($(this).parents('tr')).data();
        $('#action').val('edit');
        $('#id_kredit').val(data.id_kredit);
        $('#nama-anggota').hide();
        $('#field-idanggota').show();
        $('#id_anggota').val(data.id_anggota);
        $('#nik').val(data.nik);
        $('#tlp').val(data.telpon);
        $('#kotaLahir').val(data.kota_lahir);
        $('#tglLahir').val(data.tanggal_lahir);
        $('#address').val(data.alamat);
        $('#tgl').val(data.tanggal);
        $('#status').val(data.status);
        var formatedSaldo = data.saldo.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        var formatedPinjaman = data.jumlah_pinjaman.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        var formatedAngsuran = data.angsuran_perbulan.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        var formatedAdmin = data.administrasi.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        $('#saldo').val(formatedSaldo);
        $('#jumlah_pinjaman').val(formatedPinjaman);
        $('#tenor').val(data.lama_cicilan);
        $('#angsuran').val(formatedAngsuran);
        $('#admin').val(formatedAdmin);
    });
    // Handle cancel button click
    $('#btnBatal').on('click', function() {
        setBatal();
    });
    $('#btnSimpan').on('click', function() {
        try {
            var action = $('#action').val();
            var id_kredit = $('#id_kredit').val();
            var id_anggota = $('#id_anggota').val();
            var name_anggota = $('#name_anggota').val();
            var nik = $('#nik').val();
            var tlp = $('#tlp').val();
            var kotaLahir = $('#kotaLahir').val();
            var tglLahir = $('#tglLahir').val();
            var alamat = $('#address').val();
            var tgl = $('#tgl').val();
            var saldo = $('#saldo').val().replace(/\./g, '');
            var jumlah_pinjaman = $('#jumlah_pinjaman').val().replace(/\./g, '');
            var admin = $('#admin').val().replace(/\./g, '');
            var tenor = $('#tenor').val();
            var angsuran = $('#angsuran').val().replace(/\./g, '');
            var status = $('#status').val();


            if (action === 'post') {
                var formData = {
                    id_kredit: id_kredit,
                    id_anggota: name_anggota,
                    nik: nik,
                    telpon: tlp,
                    kota_lahir: kotaLahir,
                    tanggal_lahir: tglLahir,
                    alamat: alamat,
                    tgl: tgl,
                    saldo: Number(saldo),
                    jumlah_pinjaman: Number(jumlah_pinjaman),
                    admin: Number(admin),
                    tenor: parseInt(tenor),
                    angsuran: Number(angsuran),
                    status: status
                };
                $.ajax({
                    url: 'http://localhost:1199/koperasi/app/controller/add-pinjaman-anggota.php',
                    type: 'POST',
                    data: formData,
                    success: function(response) {
                            var data = JSON.parse(response);
                            setBatal();
                                if (data.success) {
                                    table.ajax.reload();
                                    Swal.fire(
                                        'Success!',
                                        data.message,
                                        'success'
                                    );
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Failed add new record',
                                        text: data.message
                                    });
                                }
                        },
                    error: function(xhr, status, error) {
                        setBatal();
                        Swal.fire(
                            'Error!',
                            'Failed to save data: ' + xhr.responseText,
                            'error'
                        );
                    }
                });
            }
             else {
                // PR BELUM SAMPAI PROSES UPDATE
                var formData = {
                    id_kredit: id_kredit,
                    id_anggota: id_anggota,
                    tgl: tgl,
                    saldo: Number(saldo),
                    jumlah_pinjaman: Number(jumlah_pinjaman),
                    admin: Number(admin),
                    tenor: parseInt(tenor),
                    angsuran: Number(angsuran),
                    status: status
                };
                $.ajax({
                    url: 'http://localhost:1199/koperasi/app/controller/add-pinjaman-anggota.php',
                    type: 'PUT',
                    data: formData,
                    success: function(response) {
                            var data = JSON.parse(response);
                            setBatal();
                                if (data.success) {
                                    table.ajax.reload();
                                    Swal.fire(
                                        'Success!',
                                        data.message,
                                        'success'
                                    );
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Failed Update record',
                                        text: data.message
                                    });
                                }
                        },
                    error: function(xhr, status, error) {
                        setBatal();
                        Swal.fire(
                            'Error!',
                            'Failed to save data: ' + xhr.responseText,
                            'error'
                        );
                    }
                });
            };
        } catch (error) {
            console.error('Error:', err);
        }
    });
    $('#dataPetugas tbody').on('click', '.btnDelete', function() {
        var data = table.row($(this).parents('tr')).data();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
                if (result.isConfirmed) { 
                    var id_kredit = data.id_kredit;
                    $.ajax({
                        url: 'http://localhost:1199/koperasi/app/controller/add-pinjaman-anggota.php',
                        type: 'DELETE',
                        data: { id_kredit: id_kredit },
                        success: function(response) {
                            var data = JSON.parse(response);
                            if (data.success) {
                                table.ajax.reload();
                                Swal.fire(
                                    'Success!',
                                    data.message,
                                    'success'
                                );
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Failed delete selected record',
                                    text: data.message
                                });
                            }
                        },
                        error: function(xhr, status, error) {
                            Swal.fire({
                                title: 'Error Warning !',
                                text: "You Have Some Error" + error,
                                icon: 'warning',
                            })
                        }
                    });
                }
            });
        });
} );