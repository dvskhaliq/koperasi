$(document).ready(async function() {
    let nominal_angsuran;
    let totalPembayaran;
    var table;
    let sisa_angsuran;
    let id_kredit = $('#id_kredit').val();
    let id_anggota = $('#id_anggota').val();
    var id = "ANGS" + Math.random().toString(36).slice(2);
    $('#id_angsuran').val(id);
    $('#action').val('post'); 

    if (session === 'Users') {
        table = $('#dataPetugas').DataTable({
            "ajax": "http://localhost:1199/koperasi/app/controller/fetch-angsuran.php?kredit="+ id_kredit +"&anggota="+ id_anggota + "",
            "columns":[
                { "data": "id_kredit" }, 
                { "data": "id_anggota" },
                { "data": "id_angsuran" },
                { "data": "nama_anggota" },
                { "data": "jumlah_pinjaman",
                  "render": function(data, type, row) {
                        return new Intl.NumberFormat('id-ID', {
                           style: 'currency',
                           currency: 'IDR'
                       }).format(data);
                   }
                },
                { "data": "administrasi",
                  "render": function(data, type, row) {
                        return new Intl.NumberFormat('id-ID', {
                           style: 'currency',
                           currency: 'IDR'
                       }).format(data);
                   }
                },
                { "data": "nominal_pembayaran",
                  "render": function(data, type, row) {
                        return new Intl.NumberFormat('id-ID', {
                           style: 'currency',
                           currency: 'IDR'
                       }).format(data);
                   }
                },
                { "data": "terlambat" },
                { "data": "denda",
                  "render": function(data, type, row) {
                        return new Intl.NumberFormat('id-ID', {
                           style: 'currency',
                           currency: 'IDR'
                       }).format(data);
                   }
                },
                { "data": "tanggal_bayar" },
                { "data": "tgl_kredit" },
                { "data": "total_pembayaran",
                  "render": function(data, type, row) {
                        return new Intl.NumberFormat('id-ID', {
                           style: 'currency',
                           currency: 'IDR'
                       }).format(data);
                   }
                },
                { "data": "status" },
                { "data": "tenor_bulan" },
                { "data": "sisa_angsuran",
                  "render": function(data, type, row) {
                        return new Intl.NumberFormat('id-ID', {
                           style: 'currency',
                           currency: 'IDR'
                       }).format(data);
                   }
                },
                { "data": "keterangan" },
                { "data": null,
                  "render": function(data, type, row) {
                                 return `<a class='btn btn-primary btn-sm btnBuktiTransaksi' href='bukti-transaksi.php?file=${row.bukti_transaksi}'>Unduh</a>`;
                            }
                },
                { 
                    "data": null, 
                    "defaultContent": "<button class='btn btn-danger btn-sm btnDelete'>Hapus</button>" 
                }
            ]
        });   
    } else {
        table = $('#dataPetugas').DataTable({
            "ajax": "http://localhost:1199/koperasi/app/controller/fetch-angsuran.php?kredit="+ id_kredit +"&anggota="+ id_anggota + "",
            "columns":[
                { "data": "id_kredit" }, 
                { "data": "id_anggota" },
                { "data": "id_angsuran" },
                { "data": "nama_anggota" },
                { "data": "jumlah_pinjaman",
                  "render": function(data, type, row) {
                        return new Intl.NumberFormat('id-ID', {
                           style: 'currency',
                           currency: 'IDR'
                       }).format(data);
                   }
                },
                { "data": "administrasi",
                  "render": function(data, type, row) {
                        return new Intl.NumberFormat('id-ID', {
                           style: 'currency',
                           currency: 'IDR'
                       }).format(data);
                   }
                },
                { "data": "nominal_pembayaran",
                  "render": function(data, type, row) {
                        return new Intl.NumberFormat('id-ID', {
                           style: 'currency',
                           currency: 'IDR'
                       }).format(data);
                   }
                },
                { "data": "terlambat" },
                { "data": "denda",
                  "render": function(data, type, row) {
                        return new Intl.NumberFormat('id-ID', {
                           style: 'currency',
                           currency: 'IDR'
                       }).format(data);
                   }
                },
                { "data": "tanggal_bayar" },
                { "data": "tgl_kredit" },
                { "data": "total_pembayaran",
                  "render": function(data, type, row) {
                        return new Intl.NumberFormat('id-ID', {
                           style: 'currency',
                           currency: 'IDR'
                       }).format(data);
                   }
                },
                { "data": "status" },
                { "data": "tenor_bulan" },
                { "data": "sisa_angsuran",
                  "render": function(data, type, row) {
                        return new Intl.NumberFormat('id-ID', {
                           style: 'currency',
                           currency: 'IDR'
                       }).format(data);
                   }
                },
                { "data": "keterangan" },
                { "data": null,
                    "render": function(data, type, row) {
                                 return `<a class='btn btn-primary btn-sm btnBuktiTransaksi' href='bukti-transaksi.php?file=${row.bukti_transaksi}'>Unduh</a>`;
                              }
                },
                { 
                    "data": null, 
                    "defaultContent": "<button class='btn btn-danger btn-sm btnDelete'>Hapus</button><button class='btn btn-success btn-sm editBtn'>Edit</button>" 
                }
            ]
        });
    }

function moneyFormatForm(data) {
        var formattedVal = new Intl.NumberFormat('id-ID').format(data);
        return formattedVal;
    }
    // Fungsi untuk menambahkan bulan dengan penanganan tanggal
function addMonths(date, months) {
    const d = new Date(date);
    d.setMonth(d.getMonth() + months);
    
    // Jika tanggal hasilnya lebih dari hari terakhir bulan, set tanggal ke hari terakhir bulan tersebut
    if (d.getDate() < date.getDate()) {
        d.setDate(0); // Mengatur tanggal ke hari terakhir bulan sebelumnya
    }
    
    return d;
}

// Fungsi untuk memformat tanggal menjadi 'yyyy-MM-dd'
function formatDate(date) {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    return `${year}-${month}-${day}`;
}

async function getTempoDate(startDate, tenor, id_kredit, id_anggota) {
    const today = new Date(startDate);
    const monthsToAdd = parseInt(tenor);
    let allDates = [];
    let dates = [];
     
    // Membungkus AJAX call dalam Promise
    const response = await new Promise((resolve, reject) => {
        $.ajax({
            url: 'http://localhost:1199/koperasi/app/controller/add-angsuran.php',
            method: 'GET',
            data: { id_kredit: id_kredit, id_anggota: id_anggota, param: 'checkTempo', checkDate: startDate },
            success: function(response) {
                resolve(response);
            },
            error: function(xhr, status, error) {
                reject(new Error('Failed to fetch data: ' + xhr.responseText));
            }
        });
    });
    
    const data = JSON.parse(response);
    if (data.kredit_lunas.length > 0) {
        // Generate dates for each month and ensure validity
        for (let i = 0; i < monthsToAdd; i++) {
            const newDate = addMonths(today, i + 1);
            const formattedDate = formatDate(newDate);
            allDates.push(formattedDate);
        }
        // Filter out dates that are already in kredit_lunas
        for (let index = 0; index < allDates.length; index++) {
            for (let u = 0; u < data.kredit_lunas.length; u++) {
                if (allDates[index] !== data.kredit_lunas[u]) {
                    dates.push(String(allDates[index]));
                }
            }
        }
    } else {
        const nextMonthDate = addMonths(today, 1);
        const formattedDate = formatDate(nextMonthDate);
        dates.push(formattedDate);
    }
    
    // Mengubah array dates menjadi JSON
    return JSON.stringify({ tempo : dates });
}
    function calculateDateInterval(startDate, endDate) {
        // Menghitung selisih waktu dalam milidetik
        const diffInMilliseconds = endDate - startDate;

        // Konversi selisih waktu menjadi hari
        const diffInDays = diffInMilliseconds / (1000 * 3600 * 24);

        // Mengembalikan nilai interval dalam hari (dibulatkan ke bawah)
        return Math.floor(diffInDays);
    }

    function setBatal () {
        $('#petugasForm')[0].reset();
        id_kredit = $('#id_kredit').val();
        id_anggota = $('#id_anggota').val();
        var id = "ANGS" + Math.random().toString(36).slice(2);
        $('#id_angsuran').val(id);
    }

    function setDefaultValue() {
        var action = $('#action').val();
        if (id_kredit && id_kredit !== '' && action === 'post') {
            try { 
                $.ajax({
                    url: 'http://localhost:1199/koperasi/app/controller/add-angsuran.php', // URL endpoint untuk mendapatkan total transaksi
                    method: 'GET',
                    data: { id_kredit: id_kredit, id_anggota: id_anggota, param: 'initFetch' },
                    success: async function(response) {
                        var data = JSON.parse(response);
                        var totalPinjaman = Number(data.data.jumlah_pinjaman) + Number(data.data.administrasi);
                        var formated = moneyFormatForm(totalPinjaman);
                        var DateTempo = await getTempoDate(data.data.tanggal, data.data.lama_cicilan, id_kredit, id_anggota);
                        DateTempo = JSON.parse(DateTempo);
                        nominal_angsuran = data.data.angsuran_perbulan;
                        sisa_angsuran = data.data.sisa_angsuran;
                        var tenor_ke = Number(data.data.tenor_dibayar);
                        $('#tgl_kredit').val(DateTempo.tempo[0]);
                        $('#jumlah_pinjaman').val(formated);
                        $('#tenor_dibayar').val(tenor_ke);
                        if (data.data.tenor_dibayar > 0) {
                            $('#btnSimpan').prop('disabled', false);
                        } else {
                            $('#btnSimpan').prop('disabled', true);
                        }
                    },
                    error: function(xhr, status, error) {
                        Swal.fire(
                            'Error!',
                            'Failed to save data: ' + xhr.responseText,
                            'error'
                        );
                    }
              });
            } catch (error) {
                console.error('Error:', err);
            }
        }
    }
    setDefaultValue();
    var actions = $('#action').val();
    if (actions === 'post') {
        $('#tgl').on('input', function() {
            var tgl_bayar = new Date($(this).val());
            var kreditDate = new Date($('#tgl_kredit').val());
            var datediff = calculateDateInterval(kreditDate, tgl_bayar);
            var bayar = moneyFormatForm(nominal_angsuran);
            var sisa_bayar = moneyFormatForm(sisa_angsuran);
            $('#nominal_angsuran').val(bayar)
            $('#sisa_bayar').val(sisa_bayar);
                if (datediff >= 1) {
                    var denda = 0.05
                    var dendaKeterlambatan =  Number(datediff) * (Number(nominal_angsuran) * Number(denda));
                    dendaKeterlambatan = Math.ceil(dendaKeterlambatan);
                    var totalPembayaran = Number(nominal_angsuran) + dendaKeterlambatan;
                    totalPembayaran = Math.ceil(totalPembayaran);
                    totalPembayaran = moneyFormatForm(totalPembayaran);
                    var formattedDenda = moneyFormatForm(dendaKeterlambatan);
                    $('#denda').val(formattedDenda);
                    $('#terlambat').val(datediff);
                    $('#total_bayar').val(totalPembayaran)
                } else {
                    totalPembayaran = moneyFormatForm(nominal_angsuran);
                    $('#denda').val('0');
                    $('#terlambat').val('0');
                    $('#total_bayar').val(totalPembayaran)
                }
        });   
    }

    $('#btnSimpan').on('click', function() {
        try {
            var id_angsuran = $('#id_angsuran').val();
            var id_kredit = $('#id_kredit').val();
            var id_anggota = $('#id_anggota').val();
            var nominal_angsuran = $('#nominal_angsuran').val().replace(/\./g, '');
            var tgl = $('#tgl_kredit').val()
            var tgl_bayar = $('#tgl').val();
            var denda = $('#denda').val().replace(/\./g, '');;
            var total_bayar = $('#total_bayar').val().replace(/\./g, '');;
            var terlambat = $('#terlambat').val();
            var keterangan = $('#keterangan').val();
            var tenor_bulan = $('#tenor_dibayar').val();
            var file_data = $('#file').prop('files')[0]; // Ambil file yang diupload
            var actions = $('#action').val();
            var status;
            if (session === 'Admin') {
                status = $('#status').val();
            } else {
                status = "pending";
            }
            const formData = new FormData();
            formData.append('id_angsuran', id_angsuran);
            formData.append('action', actions);
            formData.append('id_kredit', id_kredit);
            formData.append('id_anggota', id_anggota);
            formData.append('nominal_angsuran', Number(nominal_angsuran));
            formData.append('tgl_kredit', tgl);
            formData.append('tgl_bayar', tgl_bayar);
            formData.append('denda', denda);
            formData.append('total_bayar', total_bayar);
            formData.append('terlambat', Number(terlambat));
            formData.append('tenor_bulan', Number(tenor_bulan));
            formData.append('keterangan', keterangan);
            formData.append("bukti_transaksi", file_data);
            formData.append("status", status);
                $.ajax({
                    url: 'http://localhost:1199/koperasi/app/controller/add-angsuran.php',
                    method: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    enctype: 'multipart/form-data',
                    success: function(response) {
                        var data = JSON.parse(response);
                            if (data.success) {
                                table.ajax.reload();
                                Swal.fire(
                                    'Success!',
                                    data.message,
                                    'success'
                                );
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Failed add new record',
                                    text: data.message
                                });
                            }
                        },
                    error: function(xhr, status, error) {
                        Swal.fire(
                            'Error!',
                            'Failed to save data: ' + xhr.responseText,
                            'error'
                        );
                    }
                });  
        } catch (error) {
            console.error('Error:', err);
        }
    });
    $('#btnBatal').on('click', function() {
        setBatal();
        setDefaultValue();
    });
    $('#dataPetugas tbody').on('click', '.editBtn', function () {
        var data = table.row($(this).parents('tr')).data();
        $('#action').val('put');
        $('#id_angsuran').attr('readonly', true);
        $('#id_kredit').attr('readonly', true);
        $('#id_anggota').attr('readonly', true);
        $('#tgl_kredit').attr('readonly', true);
        $('#jumlah_pinjaman').attr('readonly', true);
        $('#nominal_angsuran').attr('readonly', true);
        $('#denda').attr('readonly', true);
        $('#sisa_bayar').attr('readonly', true);
        $('#tenor_dibayar').attr('readonly', true);
        $('#terlambat').attr('readonly', true);
        $('#sisa_bayar').attr('readonly', true);
        $('#terlambat').attr('readonly', true);
        $('#total_bayar').attr('readonly', true);
        $('#tgl').attr('readonly', true);


        $('#id_angsuran').val(data.id_angsuran);
        $('#id_kredit').val(data.id_kredit);
        $('#id_anggota').val(data.id_anggota);
        $('#tgl_kredit').val(data.tgl_kredit);
        var jumlah_pinjaman = moneyFormatForm(data.jumlah_pinjaman);
        $('#jumlah_pinjaman').val(jumlah_pinjaman);
        $('#keterangan').val(data.keterangan);
        var nominal_angsuran = moneyFormatForm(data.nominal_pembayaran);
        $('#nominal_angsuran').val(nominal_angsuran);
        var denda = moneyFormatForm(data.denda);
        $('#denda').val(denda);
        var sisa_bayar = moneyFormatForm(data.sisa_bayar);
        $('#sisa_bayar').val(sisa_bayar);
        $('#terlambat').val(data.terlambat);
        $('#status').val(data.status);
        $('#sisa_bayar').val(data.sisa_angsuran);
        var total_pembayaran = moneyFormatForm(data.total_pembayaran);
        $('#total_bayar').val(total_pembayaran);
        $('#tenor_dibayar').val(data.tenor_bulan);
        $('#tgl').val(data.tanggal_bayar);
    });
    $('#dataPetugas tbody').on('click', '.btnDelete', function() {
        var data = table.row($(this).parents('tr')).data();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
                if (result.isConfirmed) { 
                    var id_kredit = data.id_kredit;
                    $.ajax({
                        url: 'http://localhost:1199/koperasi/app/controller/add-angsuran.php',
                        method: 'DELETE',
                        data: { id_kredit: id_kredit },
                        success: function(response) {
                            var data = JSON.parse(response);
                            if (data.success) {
                                table.ajax.reload();
                                Swal.fire(
                                    'Success!',
                                    data.message,
                                    'success'
                                );
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Failed delete selected record',
                                    text: data.message
                                });
                            }
                        },
                        error: function(xhr, status, error) {
                            Swal.fire({
                                title: 'Error Warning !',
                                text: "You Have Some Error" + error,
                                icon: 'warning',
                            })
                        }
                    });
                }
            });
        });
});