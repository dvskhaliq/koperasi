$(document).ready(function() {
    // Deklarasikan variabel table di sini agar bisa diakses di seluruh fungsi
    let table;
    // check session user
    $.ajax({
        url: 'http://localhost:1199/koperasi/app/controller/fetch-session.php',
        method: 'GET',
        success: function(response) {
            var result = JSON.parse(response);
            if (result.loggedIn && result.role === 'Admin') {
                // User logged in
                table = $('#dataPetugas').DataTable(  {
                    "ajax": "http://localhost:1199/koperasi/app/controller/fetch-pengambilan.php",
                    "columns":[
                        { "data": "id_transaksi" },
                        { "data": "id_anggota" },
                        { "data": "nama_anggota" },
                        { "data": "tanggal" },
                        { "data": "simpanan_sukarela",
                          "render": function(data, type, row) {
                                         return new Intl.NumberFormat('id-ID', {
                                            style: 'currency',
                                            currency: 'IDR'
                                        }).format(data);
                                    }
                        },
                        { "data": "jumlah_penarikan",
                          "render": function(data, type, row) {
                                           return new Intl.NumberFormat('id-ID', {
                                              style: 'currency',
                                              currency: 'IDR'
                                          }).format(data);
                                      }
                        },
                        {"data": "status"},
                        { 
                            "data": null, 
                            "defaultContent": "<button class='btn btn-success btn-sm editBtn'>Edit</button><button class='btn btn-danger btn-sm btnDelete'>Hapus</button>" 
                        }
                    ]
                });
            } else if(result.loggedIn && result.role === 'Users') {
                table = $('#dataPenarikanUsers').DataTable(  {
                    "ajax": "http://localhost:1199/koperasi/app/controller/fetch-pengambilan.php",
                    "columns":[
                        { "data": "id_transaksi" },
                        { "data": "id_anggota" },
                        { "data": "nama_anggota" },
                        { "data": "tanggal" },
                        { "data": "simpanan_sukarela",
                          "render": function(data, type, row) {
                                         return new Intl.NumberFormat('id-ID', {
                                            style: 'currency',
                                            currency: 'IDR'
                                        }).format(data);
                                    }
                        },
                        { "data": "jumlah_penarikan",
                          "render": function(data, type, row) {
                                           return new Intl.NumberFormat('id-ID', {
                                              style: 'currency',
                                              currency: 'IDR'
                                          }).format(data);
                                      }
                        },
                        {"data": "status"},
                        { 
                            "data": null, 
                            "defaultContent": "<button class='btn btn-danger btn-sm btnDelete'>Hapus</button>" 
                        }
                    ]
                });
            }else {
                // User not logged in, redirect to login page or show message
                window.location.href = '../index.php'; // Redirect ke halaman login
            }
        },
        error: function(xhr, status, error) {
            // Tindakan lain jika terjadi error
            Swal.fire(
                'Error!',
                'Terjadi kesalahan saat memeriksa session user: ' + error,
                'error'
            );
        }
    });
    function loadAnggotaList() {
        $.ajax({
            url: 'http://localhost:1199/koperasi/app/controller/fetch-anggota.php',
            method: 'GET',
            success: function(data) {
                var anggotaList = JSON.parse(data);
                $('#name_anggota').empty();
                $('#name_anggota').append('<option>Silahkan anda pilih akun anggota</option>');
                $.each(anggotaList, function(key, value) {
                    $('#name_anggota').append('<option value="'+ value.id_anggota +'">'+ value.nama_anggota +'</option>');
                });
            }
        });
    };
    loadAnggotaList();
    function setdefault () {
        $('#petugasForm')[0].reset();
        var id = "TRK" + Math.random().toString(36).slice(2);
        $('#id_penarikan').val(id);
        $('#action').val('post');
        $('#nama-anggota').show();
        $('#field-idanggota').hide();
    };
    setdefault();
    $('#dataPetugas tbody').on('click', '.editBtn', function () {
        var data = table.row($(this).parents('tr')).data();
        $('#action').val('edit');
        $('#id_penarikan').val(data.id_transaksi);
        $('#nama-anggota').hide();
        $('#field-idanggota').show();
        $('#id_anggota').val(data.id_anggota);
        $('#tgl').val(data.tanggal);
        var formatedSimpanan = data.simpanan_sukarela.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        var formatedPenarikan = data.jumlah_penarikan.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        $('#simpanan_sukarela').val(formatedSimpanan);
        $('#jumlah_penarikan').val(formatedPenarikan);
    });
    function setBatal () {
        var action = $('#action').val();
        if (action === 'edit') {
            setdefault();
            loadAnggotaList();
        }else {
            setdefault();
        }
    };
    function moneyFormatForm(data) {
        var formattedVal = new Intl.NumberFormat('id-ID').format(data);
        return formattedVal;
    };
    $('#jumlah_penarikan').on('input', function() {
        var inputVal = $(this).val();
        var numericVal = inputVal.replace(/\./g, '');
        var formattedVal = moneyFormatForm(numericVal);
        $(this).val(formattedVal);
    });
    $('#name_anggota').change(function() {
        var anggotaId = $(this).val();
        if (anggotaId) {
        $.ajax({
            url: 'http://localhost:1199/koperasi/app/controller/add-pengambilan-saldo.php', // URL endpoint untuk mendapatkan total transaksi
            method: 'GET',
            data: { id_anggota: anggotaId, action: 'saldo' },
            success: function(response) {
                var data = JSON.parse(response);
                // Lakukan sesuatu dengan data yang diterima, misalnya menampilkan total transaksi
                var number_string = data.sisaSaldo.replace(/[^,\d]/g, '').toString();
                var formated = moneyFormatForm(number_string);
                $('#simpanan_sukarela').val(formated);
            },
            error: function(xhr, status, error) {
                console.error({Error: err, Status:  status, xhr: xhr});
            }
      });
    }
    });
    $('#btnSimpan').on('click', function() {
        try {
            var action = $('#action').val();
            var id_penarikan = $('#id_penarikan').val();
            var name_anggota = $('#name_anggota').val();
            var id_anggota = $('#id_anggota').val();
            var tgl = $('#tgl').val();
            var simpanan_sukarela = $('#simpanan_sukarela').val().replace(/\./g, '');
            var jumlah_penarikan = $('#jumlah_penarikan').val().replace(/\./g, '');
            var status = $('#status').val();

            if (action === 'post') {
                if (Number(simpanan_sukarela) > Number(jumlah_penarikan)) {
                    var formData = {
                        id_penarikan: id_penarikan,
                        id_anggota: name_anggota,
                        tgl: tgl,
                        simpanan_sukarela: Number(simpanan_sukarela),
                        jumlah_penarikan: Number(jumlah_penarikan),
                        status: status,
                    };
                    $.ajax({
                        url: 'http://localhost:1199/koperasi/app/controller/add-pengambilan-saldo.php',
                        type: 'POST',
                        data: formData,
                        success: function(response) {
                                var data = JSON.parse(response);
                                setBatal();
                                    if (data.success) {
                                        table.ajax.reload();
                                        Swal.fire(
                                            'Success!',
                                            data.message,
                                            'success'
                                        );
                                    } else {
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Failed add new record',
                                            text: data.message
                                        });
                                    }
                                    setdefault ()
                            },
                        error: function(xhr, status, error) {
                            setBatal();
                            Swal.fire(
                                'Error!',
                                'Failed to save data: ' + xhr.responseText,
                                'error'
                            );
                        }
                    });
                } else {
                    setBatal();
                    Swal.fire(
                        'Error!',
                        'Sisa saldo anda tidak cukup. ',
                        'error'
                    );
                }
            } else {
                var formData = {
                    id_penarikan: id_penarikan,
                    id_anggota: id_anggota,
                    tgl: tgl,
                    simpanan_sukarela: Number(simpanan_sukarela),
                    jumlah_penarikan: Number(jumlah_penarikan),
                    status: status,
                };
                $.ajax({
                    url: 'http://localhost:1199/koperasi/app/controller/add-pengambilan-saldo.php',
                    type: 'PUT',
                    data: formData,
                    success: function(response) {
                            var data = JSON.parse(response);
                            setBatal();
                                if (data.success) {
                                    table.ajax.reload();
                                    Swal.fire(
                                        'Success!',
                                        data.message,
                                        'success'
                                    );
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Failed Update record',
                                        text: data.message
                                    });
                                }
                        },
                    error: function(xhr, status, error) {
                        setBatal();
                        Swal.fire(
                            'Error!',
                            'Failed to save data: ' + xhr.responseText,
                            'error'
                        );
                    }
                });
            };
        } catch (error) {
            console.error('Error:', err);
        }
    });
    $('#btnBatal').on('click', function() {
        setBatal();
    });
    $('#dataPetugas tbody').on('click', '.btnDelete', function() {
        var data = table.row($(this).parents('tr')).data();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
                if (result.isConfirmed) { 
                    var id_transaksi = data.id_transaksi;
                    $.ajax({
                        url: 'http://localhost:1199/koperasi/app/controller/add-pengambilan-saldo.php',
                        type: 'DELETE',
                        data: { id_transaksi: id_transaksi },
                        success: function(response) {
                            var data = JSON.parse(response);
                            if (data.success) {
                                table.ajax.reload();
                                Swal.fire(
                                    'Success!',
                                    data.message,
                                    'success'
                                );
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Failed delete selected record',
                                    text: data.message
                                });
                            }
                        },
                        error: function(xhr, status, error) {
                            Swal.fire({
                                title: 'Error Warning !',
                                text: "You Have Some Error" + error,
                                icon: 'warning',
                            })
                        }
                    });
                }
            });
        });
});