$(document).ready(function() {
        // Initialize DataTables
        var table = $('#dataPetugas').DataTable({
            "ajax": "http://localhost:1199/koperasi/app/controller/info-anggota.php",
            "columns": [
                { "data": "id_anggota" },
                { "data": "nama_anggota" },
                { "data": "telpon" },
                {"data": "kota_lahir"},
                {"data": "tanggal_lahir"},
                { "data": "alamat" },
                { 
                    "data": null, 
                    "defaultContent": "<button class='btn btn-success btn-sm editBtn'>Edit</button><button class='btn btn-danger btn-sm btnDelete'>Hapus</button>" 
                }
            ]
        });
        $('#action').val('post');

        function loadAnggotaList() {
            $.ajax({
                url: 'http://localhost:1199/koperasi/app/controller/add-info-petugas.php',
                method: 'GET',
                success: function(data) {
                    var anggotaList = JSON.parse(data);
                    $('#id_anggota').empty();
                    $('#id_anggota').append('<option> Silahkan anda pilih akun anggota</option>');
                    $.each(anggotaList, function(key, value) {
                        $('#id_anggota').append('<option value="'+ value.id_anggota +'">'+ value.username +'</option>');
                    });
                }
            });
        }
        function setReset() {
            var action = $('#action').val();
            if (action === 'edit') {
                $('#petugasForm')[0].reset();
                $('#action').val('post');
                $('#id_anggota').replaceWith('<select class="form-control" id="id_anggota" name="id_anggota" required><select>');
                loadAnggotaList();
            }else {
                $('#petugasForm')[0].reset();
                $('#action').val('post');
            }
        }
        loadAnggotaList();

    // Handle form submission
    $('#btnSimpan').on('click', function() {
        var action = $('#action').val();
        if (action === 'post') {
            var id_anggota = $('#id_anggota').val();
            var namaAnggota = $('#namaAnggota').val();
            var kotaLahir = $('#kotaLahir').val();
            var tglLahir = $('#tglLahir').val();
            var tlp = $('#tlp').val();
            var address = $('#address').val(); 
    
            $.ajax({
                url: 'http://localhost:1199/koperasi/app/controller/add-info-petugas.php',
                type: 'POST',
                data: {
                    id_anggota: id_anggota,
                    namaAnggota: namaAnggota,
                    kotaLahir: kotaLahir,
                    tglLahir: tglLahir,
                    tlp: tlp,
                    address: address
                },
                success: function(data) {
                    var response = JSON.parse(data);
                    if (response.success == true) {
                        Swal.fire(
                            'Success!',
                            response.message,
                            'success'
                        );
                    } else {
                        Swal.fire({
                            title: 'Error Warning !',
                            text: response.message,
                            icon: 'warning',
                        }) 
                    }
                    table.ajax.reload();
                    setReset();
                },
                error: function(xhr, status, error) {
                    Swal.fire(
                        'Error!',
                        'Failed to save data: ' + xhr.responseText,
                        'error'
                    );
                }
            });
        } else {
            var url = 'http://localhost:1199/koperasi/app/controller/add-info-petugas.php';
            var method ='PUT';
            
            $.ajax({ 
                url: url,
                method: method,
                data: $('#petugasForm').serialize(),
                success: function(response) {
                    table.ajax.reload();
                    setReset();
                    Swal.fire(
                        'Success!',
                        'Your data has been saved.',
                        'success'
                    );
                },
                error: function(xhr, status, error) {
                    setReset();
                    Swal.fire(
                        'Error!',
                        'Failed to save data: ' + xhr.responseText,
                        'error'
                    );
                }
            });
        }
    });

    $('#dataPetugas tbody').on('click', '.editBtn', function () {
        var data = table.row($(this).parents('tr')).data();
        $('#action').val('edit');
        $('#id_anggota').replaceWith('<input type="text" class="form-control" id="id_anggota" name="id_anggota" readonly>');
        $('#id_anggota').val(data.id_anggota);
        $('#kotaLahir').val(data.kota_lahir);
        $('#tglLahir').val(data.tanggal_lahir);
        $('#namaAnggota').val(data.nama_anggota);
        $('#tlp').val(data.telpon);
        $('#address').val(data.alamat);
    });

    $('#dataPetugas tbody').on('click', '.btnDelete', function() {
        var data = table.row($(this).parents('tr')).data();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
                if (result.isConfirmed) { 
                    var id_anggota = data.id_anggota;
                    $.ajax({
                        url: 'http://localhost:1199/koperasi/app/controller/add-info-petugas.php',
                        type: 'DELETE',
                        data: { id_anggota: id_anggota },
                        success: function(response) {
                            table.ajax.reload();
                        },
                        error: function(xhr, status, error) {
                            Swal.fire({
                                title: 'Error Warning !',
                                text: "You Have Some Error" + error,
                                icon: 'warning',
                            })
                        }
                    });
                }
            });
        });

            // // Handle cancel button click
    $('#btnBatal').on('click', function() {
        setReset();
    });
})