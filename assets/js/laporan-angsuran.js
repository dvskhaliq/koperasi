$(document).ready(function() {
    var table = $('#dataAngsuran').DataTable({
        "ajax": "http://localhost:1199/koperasi/app/controller/fetch-angsuran.php?kredit="+ id_kredit +"&anggota="+ id_anggota + "",
        "columns":[
            { "data": "id_kredit" }, 
            { "data": "id_anggota" },
            { "data": "nama_anggota" },
            { "data": "jumlah_pinjaman",
              "render": function(data, type, row) {
                    return new Intl.NumberFormat('id-ID', {
                       style: 'currency',
                       currency: 'IDR'
                   }).format(data);
               }
            },
            { "data": "administrasi",
              "render": function(data, type, row) {
                    return new Intl.NumberFormat('id-ID', {
                       style: 'currency',
                       currency: 'IDR'
                   }).format(data);
               }
            },
            { "data": "nominal_pembayaran",
              "render": function(data, type, row) {
                    return new Intl.NumberFormat('id-ID', {
                       style: 'currency',
                       currency: 'IDR'
                   }).format(data);
               }
            },
            { "data": "terlambat" },
            { "data": "denda",
              "render": function(data, type, row) {
                    return new Intl.NumberFormat('id-ID', {
                       style: 'currency',
                       currency: 'IDR'
                   }).format(data);
               }
            },
            { "data": "tanggal_bayar" },
            { "data": "total_pembayaran",
              "render": function(data, type, row) {
                    return new Intl.NumberFormat('id-ID', {
                       style: 'currency',
                       currency: 'IDR'
                   }).format(data);
               }
            },
            { "data": "status" },
            { "data": "tenor_bulan" },
            { "data": "sisa_angsuran",
              "render": function(data, type, row) {
                    return new Intl.NumberFormat('id-ID', {
                       style: 'currency',
                       currency: 'IDR'
                   }).format(data);
               }
            },
            { "data": "keterangan" },
        ]
    });
});