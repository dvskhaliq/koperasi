$(document).ready(function() {
    // Deklarasikan variabel table di sini agar bisa diakses di seluruh fungsi
    let table;
    // check session user
        $.ajax({
            url: 'http://localhost:1199/koperasi/app/controller/fetch-session.php',
            method: 'GET',
            success: function(response) {
                var result = JSON.parse(response);
                if (result.loggedIn && result.role === 'Admin') {
                    // User logged in
                    table = $('#dataPetugas').DataTable(  {
                        "ajax": "http://localhost:1199/koperasi/app/controller/fetch-simpanan.php",
                        "columns":[
                            { "data": "id_transaksi" },
                            { "data": "id_anggota" },
                            { "data": "nama_anggota" },
                            { "data": "nominal_simpanan",
                              "render": function(data, type, row) {
                                             return new Intl.NumberFormat('id-ID', {
                                                style: 'currency',
                                                currency: 'IDR'
                                            }).format(data);
                                        }
                            },
                            { "data": "tanggal" },
                            { "data": "jenis_simpanan" },
                            { "data": "status" },
                            { 
                                "data": null,
                                "render": function(data, type, row) {
                                             return `<a class='btn btn-primary btn-sm btnBuktiTransaksi' href='bukti-transaksi.php?file=${row.bukti_transaksi}'>Unduh</a>`;
                                          }
                            },
                            { 
                                "data": null, 
                                "defaultContent": "<button class='btn btn-success btn-sm editBtn'>Edit</button><button class='btn btn-danger btn-sm btnDelete'>Hapus</button>" 
                            }
                        ]
                    });
                } else if(result.loggedIn && result.role === 'Users') {
                    table = $('#dataPetugas').DataTable(  {
                        "ajax": "http://localhost:1199/koperasi/app/controller/fetch-simpanan.php",
                        "columns":[
                            { "data": "id_transaksi" },
                            { "data": "id_anggota" },
                            { "data": "nama_anggota" },
                            { "data": "nominal_simpanan",
                              "render": function(data, type, row) {
                                             return new Intl.NumberFormat('id-ID', {
                                                style: 'currency',
                                                currency: 'IDR'
                                            }).format(data);
                                        }
                            },
                            { "data": "tanggal" },
                            { "data": "jenis_simpanan" },
                            { "data": "status" },
                            { "data": null,
                              "render": function(data, type, row) {
                                           return `<a class='btn btn-primary btn-sm btnBuktiTransaksi' href='bukti-transaksi.php?file=${row.bukti_transaksi}'>Unduh</a>`;
                                        }
                            },
                            { 
                                "data": null, 
                                "defaultContent": "<button class='btn btn-danger btn-sm btnDelete'>Hapus</button>" 
                            }
                        ]
                    });
                }else {
                    // User not logged in, redirect to login page or show message
                    window.location.href = '../index.php'; // Redirect ke halaman login
                }
            },
            error: function(xhr, status, error) {
                // Tindakan lain jika terjadi error
                Swal.fire(
                    'Error!',
                    'Terjadi kesalahan saat memeriksa session user: ' + error,
                    'error'
                );
            }
        });


    var id = "TRX" + Math.random().toString(36).slice(2);
    $('#id_transaksi').val(id);
    $('#action').val('post');


    function loadAnggotaList() {
        $.ajax({
            url: 'http://localhost:1199/koperasi/app/controller/fetch-anggota.php',
            method: 'GET',
            success: function(data) {
                var anggotaList = JSON.parse(data);
                $('#id_anggota').empty();
                $('#id_anggota').append('<option> Silahkan anda pilih akun anggota</option>');
                $.each(anggotaList, function(key, value) {
                    $('#id_anggota').append('<option value="'+ value.id_anggota +'">'+ value.nama_anggota +'</option>');
                });
            }
        });
    }
    function setBatal () {
        var action = $('#action').val();
        if (action === 'edit') {
            $('#petugasForm')[0].reset();
            var id = "TRX" + Math.random().toString(36).slice(2);
            $('#id_transaksi').val(id);
            $('#action').val('post');
            $('#id_anggota').replaceWith('<select class="form-control" id="id_anggota" name="id_anggota" required><select>');
            loadAnggotaList();
        }else {
            $('#petugasForm')[0].reset();
            var id = "TRX" + Math.random().toString(36).slice(2);
            $('#id_transaksi').val(id);
            $('#action').val('post');
        }
    }
    
    loadAnggotaList();

    $('#btnSimpan').on('click', function() {
        var action = $('#action').val();
        if (action === 'post') {
            var id_transaksi = $('#id_transaksi').val();
            var id_anggota = $('#id_anggota').val();
            var nominal = $('#nominal').val().replace(/\./g, '');
            var tgl = $('#tgl').val();
            var status = $('#status').val();
            var jenis = $('#jenis').val();
            var file_data = $('#file').prop('files')[0]; // Ambil file yang diupload

            const form = new FormData();
            form.append("id_transaksi", id_transaksi);
            form.append("id_anggota", id_anggota);
            form.append("status", status);
            form.append("nominal", nominal);
            form.append("tgl", tgl);
            form.append("jenis", jenis);
            form.append("bukti_transaksi", file_data);

            $.ajax({
                url: 'http://localhost:1199/koperasi/app/controller/add-simpanan.php',
                method: 'POST',
                processData: false,
                contentType: false,
                cache: false,
                data: form,
                enctype: 'multipart/form-data',
                success: function(response) {
                        var data = JSON.parse(response);
                        setBatal();
                            if (data.success) {
                                table.ajax.reload();
                                Swal.fire(
                                    'Success!',
                                    data.message,
                                    'success'
                                );
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Failed add new record',
                                    text: data.message
                                });
                            }
                    },
                error: function(xhr, status, error) {
                    setBatal();
                    Swal.fire(
                        'Error!',
                        'Failed to save data: ' + xhr.responseText,
                        'error'
                    );
                }
            });   
        } else if (action === 'edit') {    
            var id_transaksi = $('#id_transaksi').val();
            var id_anggota = $('#id_anggota').val();
            var status = $('#status').val();
            var nominal = $('#nominal').val().replace(/\./g, '');
            var tgl = $('#tgl').val();
            var jenis = $('#jenis').val();       
            $.ajax({
                url: 'http://localhost:1199/koperasi/app/controller/add-simpanan.php',
                method: 'PUT',
                data: {
                    id_transaksi,
                    id_anggota,
                    nominal,
                    tgl,
                    jenis,
                    status  
                },
                success: function(response) {
                    var data = JSON.parse(response);
                    table.ajax.reload();
                    setBatal();
                    if (data.success) {
                        Swal.fire(
                            'Success!',
                            data.message,
                            'success'
                        );
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed update new record',
                            text: data.message
                        });
                    }
                },
                error: function(xhr, status, error) {
                    setBatal();
                    Swal.fire(
                        'Error!',
                        'Failed to save data: ' + xhr.responseText,
                        'error'
                    );
                }
            });
        }
    });

    $('#dataPetugas tbody').on('click', '.btnDelete', function() {
        var data = table.row($(this).parents('tr')).data();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
                if (result.isConfirmed) { 
                    var id_transaksi = data.id_transaksi;
                    $.ajax({
                        url: 'http://localhost:1199/koperasi/app/controller/add-simpanan.php',
                        type: 'DELETE',
                        data: { id_transaksi: id_transaksi },
                        success: function(response) {
                            var data = JSON.parse(response);
                            if (data.success) {
                                table.ajax.reload();
                                Swal.fire(
                                    'Success!',
                                    data.message,
                                    'success'
                                );
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Failed delete selected record',
                                    text: data.message
                                });
                            }
                        },
                        error: function(xhr, status, error) {
                            Swal.fire({
                                title: 'Error Warning !',
                                text: "You Have Some Error" + error,
                                icon: 'warning',
                            })
                        }
                    });
                }
            });
        });

    $('#dataPetugas tbody').on('click', '.editBtn', function () {
        var data = table.row($(this).parents('tr')).data();
        $('#action').val('edit');
        $('#id_transaksi').val(data.id_transaksi);
        $('#id_anggota').replaceWith('<input type="text" class="form-control" id="id_anggota" name="id_anggota" readonly>');
        $('#id_anggota').val(data.id_anggota);
        $('#namaAnggota').val(data.nama_anggota);
        var formated = data.nominal_simpanan.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        $('#nominal').val(formated);
        $('#tgl').val(data.tanggal);
        $('#jenis').val(data.jenis_simpanan);
        $('#status').val(data.status);
    });
    $('#nominal').on('input', function() {
        var inputVal = $(this).val();
        var numericVal = inputVal.replace(/\./g, '');
        var formattedVal = new Intl.NumberFormat('id-ID').format(numericVal);
        $(this).val(formattedVal);
    });
    // Handle cancel button click
    $('#btnBatal').on('click', function() {
        setBatal();
    });
});