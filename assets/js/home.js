$(document).ready(function() {
    // Initialize DataTables
    var table = $('#dataPetugas').DataTable({
        "ajax": "http://localhost:1199/koperasi/app/controller/fetch-petugas.php",
        "columns": [
            { "data": "id_anggota" },
            { "data": "jabatan" },
            { "data": "username" },
            { 
                "data": null, 
                "defaultContent": "<button class='btn btn-success btn-sm editBtn'>Edit</button><button class='btn btn-danger btn-sm btnDelete'>Hapus</button>" 
            }
        ]
    });

    var id = "AGT" + Math.random().toString(36).slice(2); 
    $('#id_anggota').val(id);
    $('#action').val('post');
    
    // Handle form submission
    $('#btnSimpan').on('click', function() {
        var action = $('#action').val();

        if (action === 'post') {
            var id_anggota = $('#id_anggota').val();
            var jabatan = $('#jabatan').val();
            var username = $('#username').val();
            var password = $('#password').val();
    
            $.ajax({
                url: 'http://localhost:1199/koperasi/app/controller/add-petugas.php',
                type: 'POST',
                data: {
                    id_anggota: id_anggota,
                    jabatan: jabatan,
                    username: username,
                    password: password
                },
                success: function(response) {
                    var data = JSON.parse(response);
                        if (data.success) {
                            table.ajax.reload();
                            var action = $('#action').val();
                            if (action === 'edit') {
                                $('#petugasForm')[0].reset();
                                var id = "AGT" + Math.random().toString(36).slice(2);
                                $('#id_anggota').val(id);
                                $('#action').val('post');
                                $("#username").prop('disabled', false);
                            } else {
                                $('#petugasForm')[0].reset();
                                var id = "AGT" + Math.random().toString(36).slice(2);
                                $('#id_transaksi').val(id);
                                $('#action').val('post');
                            }
                            Swal.fire(
                                'Success!',
                                data.message,
                                'success'
                            );
                        } else {
                            if (action === 'edit') {
                                $('#petugasForm')[0].reset();
                                var id = "AGT" + Math.random().toString(36).slice(2);
                                $('#id_anggota').val(id);
                                $('#action').val('post');
                                $("#username").prop('disabled', false);
                            } else {
                                $('#petugasForm')[0].reset();
                                var id = "AGT" + Math.random().toString(36).slice(2);
                                $('#id_transaksi').val(id);
                                $('#action').val('post');
                            }
                            Swal.fire({
                                icon: 'error',
                                title: 'Failed add new record',
                                text: data.message
                            });
                        }
                },
                error: function(xhr, status, error) {
                    if (action === 'edit') {
                        $('#petugasForm')[0].reset();
                        var id = "AGT" + Math.random().toString(36).slice(2);
                        $('#id_anggota').val(id);
                        $('#action').val('post');
                        $("#username").prop('disabled', false);
                    } else {
                        $('#petugasForm')[0].reset();
                        var id = "AGT" + Math.random().toString(36).slice(2);
                        $('#id_transaksi').val(id);
                        $('#action').val('post');
                    }
                    Swal.fire(
                        'Error!',
                        'Failed to save data: ' + xhr.responseText,
                        'error'
                    );
                }
            });
        } else {
            var url = 'http://localhost:1199/koperasi/app/controller/add-petugas.php';
            var method ='PUT';
            
            $.ajax({
                url: url,
                method: method,
                data: $('#petugasForm').serialize(),
                success: function(response) {
                    var data = JSON.parse(response);
                        if (data.success) {
                            table.ajax.reload();
                            if (action === 'edit') {
                                $('#petugasForm')[0].reset();
                                var id = "AGT" + Math.random().toString(36).slice(2);
                                $('#id_anggota').val(id);
                                $('#action').val('post');
                                $("#username").prop('disabled', false);
                            } else {
                                $('#petugasForm')[0].reset();
                                var id = "AGT" + Math.random().toString(36).slice(2);
                                $('#id_transaksi').val(id);
                                $('#action').val('post');
                            }
                            Swal.fire(
                                'Success!',
                                data.message,
                                'success'
                            );
                        } else {
                            if (action === 'edit') {
                                $('#petugasForm')[0].reset();
                                var id = "AGT" + Math.random().toString(36).slice(2);
                                $('#id_anggota').val(id);
                                $('#action').val('post');
                                $("#username").prop('disabled', false);
                            } else {
                                $('#petugasForm')[0].reset();
                                var id = "AGT" + Math.random().toString(36).slice(2);
                                $('#id_transaksi').val(id);
                                $('#action').val('post');
                            }
                            Swal.fire({
                                icon: 'error',
                                title: 'Failed add new record',
                                text: data.message
                            });
                        }
                },
                error: function(xhr, status, error) {
                    if (action === 'edit') {
                        $('#petugasForm')[0].reset();
                        var id = "AGT" + Math.random().toString(36).slice(2);
                        $('#id_anggota').val(id);
                        $('#action').val('post');
                        $("#username").prop('disabled', false);
                    } else {
                        $('#petugasForm')[0].reset();
                        var id = "AGT" + Math.random().toString(36).slice(2);
                        $('#id_transaksi').val(id);
                        $('#action').val('post');
                    }
                    Swal.fire(
                        'Error!',
                        'Failed to save data: ' + xhr.responseText,
                        'error'
                    );
                }
            });
        }
    });

    $('#dataPetugas tbody').on('click', '.editBtn', async function () {
        var data = table.row($(this).parents('tr')).data();
        $('#action').val('edit');
        $('#id_anggota').val(data.id_anggota);
        $('#jabatan').val(data.jabatan);
        $('#username').val(data.username);
        $("#username").prop('disabled', true);
    });

    // Handle delete button click
    $('#dataPetugas tbody').on('click', '.btnDelete', function() {
        var data = table.row($(this).parents('tr')).data();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
                if (result.isConfirmed) { 
                    var id_anggota = data.id_anggota;
                    $.ajax({
                        url: 'http://localhost:1199/koperasi/app/controller/add-petugas.php',
                        type: 'DELETE',
                        data: { id_anggota: id_anggota },
                        success: function(response) {
                            var data = JSON.parse(response);
                            if (data.success) {
                                table.ajax.reload();
                                Swal.fire(
                                    'Success!',
                                    data.message,
                                    'success'
                                );
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Failed delete selected record',
                                    text: data.message
                                });
                            }
                        },
                        error: function(xhr, status, error) {
                            Swal.fire({
                                title: 'Error Warning !',
                                text: "You Have Some Error" + error,
                                icon: 'warning',
                            })
                        }
                    });
                }
            });
        });

    // // Handle cancel button click
    $('#btnBatal').on('click', function() {
        if (action === 'edit') {
            $('#petugasForm')[0].reset();
            var id = "AGT" + Math.random().toString(36).slice(2);
            $('#id_anggota').val(id);
            $('#action').val('post');
            $("#username").prop('disabled', false);
        } else {
            $('#petugasForm')[0].reset();
            var id = "AGT" + Math.random().toString(36).slice(2);
            $('#id_transaksi').val(id);
            $('#action').val('post');
        }
    });
});
