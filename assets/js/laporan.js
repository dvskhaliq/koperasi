$(document).ready(function() {
    $('#filterdate-simpanan').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD'
        }
    });
    $('#filterdate-penarikan').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD'
        }
    });
    $('#filterdate-pinjaman').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD'
        }
    });
    $('#filterdate-penarikan').val('');
    $('#filterdate-simpanan').val('');
    $('#filterdate-pinjaman').val('');
    // FETCH SIMPANAN ANGGOTA
    var tableSimpanan = $('#dataSimpanan').DataTable(  {
        "ajax": "http://localhost:1199/koperasi/app/controller/fetch-simpanan.php",
        "columns":[
            { "data": "id_transaksi" },
            { "data": "id_anggota" },
            { "data": "nama_anggota" },
            { "data": "nominal_simpanan",
              "render": function(data, type, row) {
                             return new Intl.NumberFormat('id-ID', {
                                style: 'currency',
                                currency: 'IDR'
                            }).format(data);
                        }
            },
            { "data": "tanggal" },
            { "data": "jenis_simpanan" },
        ]
    });
    // Event listener untuk perubahan tanggal
    $('#filterdate-simpanan').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
        filterByDate(picker.startDate, picker.endDate, tableSimpanan, 4);

        var startDate = picker.startDate.format('YYYY-MM-DD');
        var endDate = picker.endDate.format('YYYY-MM-DD');
        // Update href dengan parameter tanggal
        $('#print-report-simpanan').attr('href', 'export-simpanan.php?export=simpanan&start_date=' + startDate + '&end_date=' + endDate);
    });

    $('#filterdate-simpanan').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        $('#print-report-simpanan').attr('href', 'export-simpanan.php?export=simpanan');
        filterByDate(null, null, tableSimpanan, 4);
    });
    var tableSaldo = $('#dataSaldo').DataTable(  {
        "ajax": "http://localhost:1199/koperasi/app/controller/fetch-pengambilan.php",
        "columns":[
            { "data": "id_transaksi" },
            { "data": "id_anggota" },
            { "data": "nama_anggota" },
            { "data": "tanggal" },
            { "data": "simpanan_sukarela",
              "render": function(data, type, row) {
                             return new Intl.NumberFormat('id-ID', {
                                style: 'currency',
                                currency: 'IDR'
                            }).format(data);
                        }
            },
            { "data": "jumlah_penarikan",
              "render": function(data, type, row) {
                               return new Intl.NumberFormat('id-ID', {
                                  style: 'currency',
                                  currency: 'IDR'
                              }).format(data);
                          }
            }
        ]
    });
    // Event listener untuk perubahan tanggal
    $('#filterdate-penarikan').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            filterByDate(picker.startDate, picker.endDate, tableSaldo, 3);
    
            var startDate = picker.startDate.format('YYYY-MM-DD');
            var endDate = picker.endDate.format('YYYY-MM-DD');
            // Update href dengan parameter tanggal
            $('#print-report-saldo').attr('href', 'export-penarikan.php?export=penarikan&start_date=' + startDate + '&end_date=' + endDate);
    });
    
    $('#filterdate-penarikan').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            $('#print-report-saldo').attr('href', 'export-penarikan.php?export=penarikan');
            filterByDate(null, null, tableSaldo, 3);
    });


    var tablePinajaman = $('#dataPinjaman').DataTable(  {
        "ajax": "http://localhost:1199/koperasi/app/controller/fetch-kredit-anggota.php",
        "columns":[
            { "data": "id_kredit" },
            { "data": "nama_anggota" },
            { "data": "tanggal" },
            { "data": "saldo",
              "render": function(data, type, row) {
                             return new Intl.NumberFormat('id-ID', {
                                style: 'currency',
                                currency: 'IDR'
                            }).format(data);
                        }
            },
            { "data": "jumlah_pinjaman",
              "render": function(data, type, row) {
                             return new Intl.NumberFormat('id-ID', {
                                style: 'currency',
                                currency: 'IDR'
                            }).format(data);
                        }
            },
            { "data": "administrasi",
              "render": function(data, type, row) {
                             return new Intl.NumberFormat('id-ID', {
                                style: 'currency',
                                currency: 'IDR'
                            }).format(data);
                        }
            },
            { "data": "lama_cicilan" },
            { "data": "angsuran_perbulan",
              "render": function(data, type, row) {
                             return new Intl.NumberFormat('id-ID', {
                                style: 'currency',
                                currency: 'IDR'
                            }).format(data);
                        }
            },
            { 
                "data": null, 
                "render": function(data, type, row) {
                        return `<a class='btn btn-primary btn-sm btnAngsuran' href='laporan-angsuran.php?kredit=${row.id_kredit}&anggota=${row.id_anggota}'>Angsuran</a>`;
                }
            }
        ]
    }); 
    // Event listener untuk perubahan tanggal
    $('#filterdate-pinjaman').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            filterByDate(picker.startDate, picker.endDate, tablePinajaman, 2);
    
            var startDate = picker.startDate.format('YYYY-MM-DD');
            var endDate = picker.endDate.format('YYYY-MM-DD');
            // Update href dengan parameter tanggal
            $('#print-report-pinjaman').attr('href', 'export-pinjaman.php?export=pinjaman&start_date=' + startDate + '&end_date=' + endDate);
    });
    
    $('#filterdate-pinjaman').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            $('#print-report-pinjaman').attr('href', 'export-pinjaman.php?export=pinjaman');
            filterByDate(null, null, tablePinajaman, 2);
    });
    
    // Fungsi untuk memfilter DataTable berdasarkan rentang tanggal
    function filterByDate(startDate, endDate, table, index) {
        $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
            var date = new Date(data[index]); // Kolom "tanggal" ada di indeks ke-4

            if (!startDate && !endDate) {
                return true;
            }

            if (startDate && !endDate) {
                return date >= startDate;
            }

            if (!startDate && endDate) {
                return date <= endDate;
            }

            if (startDate && endDate) {
                return date >= startDate && date <= endDate;
            }

            return true;
        });

        table.draw();
        $.fn.dataTable.ext.search.pop();
    }
});