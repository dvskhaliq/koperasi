$(document).ready(function() {
        $('#formLogin').on('submit', function() {
        
        $.ajax({
            url: 'http://localhost:1199/koperasi/app/controller/login.php',
            type: 'POST',
            data: $(this).serialize(),
            success: function(response) {
                    var data = JSON.parse(response);
                        if (data.success) {
                            $('#formLogin')[0].reset();
                            window.location.href = 'home.php'; // Redirect ke halaman home jika sukses
                        } else {
                            $('#formLogin')[0].reset();
                            Swal.fire({
                                icon: 'error',
                                title: 'Login Gagal',
                                text: data.message
                            });
                        }
                },
            error: function(xhr, status, error) {
                    Swal.fire(
                        'Error!',
                        'Failed to save data: ' + xhr.responseText,
                        'error'
                    );
                }
            });
        });
    });