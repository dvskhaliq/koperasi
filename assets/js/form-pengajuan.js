$(document).ready(function() {
    var id = "KRDT" + Math.random().toString(36).slice(2);
    $('#id_kredit').val(id);
    $('#action').val('post');

    function loadAnggotaList() {
        $.ajax({
            url: 'http://localhost:1199/koperasi/app/controller/fetch-anggota.php',
            method: 'GET',
            success: function(data) {
                var anggotaList = JSON.parse(data);
                $('#name_anggota').empty();
                $('#name_anggota').append('<option>Silahkan anda pilih akun anggota</option>');
                $.each(anggotaList, function(key, value) {
                    $('#name_anggota').append('<option value="'+ value.id_anggota +'">'+ value.nama_anggota +'</option>');
                });
            }
        });
    }
    function moneyFormatForm(data) {
        var formattedVal = new Intl.NumberFormat('id-ID').format(data);
        return formattedVal;
    }
    function calculated() {
        var jumlah_pinjaman = $('#jumlah_pinjaman').val().replace(/\./g, '');
        var administrasi = jumlah_pinjaman * 0.03;
        var formattedValPinjaman = moneyFormatForm(jumlah_pinjaman);
        var formattedValAdmin = moneyFormatForm(administrasi);
        var response = {"jumlah_pinjaman": formattedValPinjaman, "administrasi": formattedValAdmin};
        return response;
    }
    loadAnggotaList();
    $('#tenor').on('input', function() {
        var inputTenor = $(this).val();
        console.log({"inputTenor": inputTenor});
        var inputPinjaman = $('#jumlah_pinjaman').val().replace(/\./g, '');
        var admin = $('#admin').val().replace(/\./g, '');
        if (inputTenor !== 0 || inputTenor !== '') {
            if (inputPinjaman !== '' || inputPinjaman !== 0) {
                var totalPinjaman = (Number(inputPinjaman) + Number(admin));
                var cicilan = Math.ceil( totalPinjaman / parseInt(inputTenor));
                if (cicilan > 0) {
                    var formatted = moneyFormatForm(cicilan);
                } else {
                    var formatted = 0;
                }
                $('#angsuran').val(formatted);
            }
        }
    });
    $('#jumlah_pinjaman').on('input', function() {
        var inputVal = $(this).val();
        var numericVal = inputVal.replace(/\./g, '');
        var formattedVal = moneyFormatForm(numericVal);
        var check = calculated(formattedVal);
        $(this).val(formattedVal);
        $('#admin').val(check.administrasi);
    });
    $('#angsuran').on('input', function() {
        var inputVal = $(this).val();
        var numericVal = inputVal.replace(/\./g, '');
        var formattedVal = moneyFormatForm(numericVal);
        $(this).val(formattedVal);
    });
    $('#admin').on('input', function() {
        var inputVal = $(this).val();
        var numericVal = inputVal.replace(/\./g, '');
        var formattedVal = moneyFormatForm(numericVal);
        $(this).val(formattedVal);
    });

    $('#name_anggota').change(function() {
        var anggotaId = $(this).val();
        console.error(anggotaId);
        if (anggotaId) {
        $.ajax({
            url: 'http://localhost:1199/koperasi/app/controller/add-pinjaman-anggota.php', // URL endpoint untuk mendapatkan total transaksi
            method: 'GET',
            data: { id_anggota: anggotaId, action: 'saldo' },
            success: function(response) {
                var data = JSON.parse(response);
                console.log(data);
                if (data.alamat !== null) {
                    var number_string = data.saldo.replace(/[^,\d]/g, '').toString();
                    var formated = moneyFormatForm(number_string);
                    $('#tlp').val(data.telpon);
                    $('#address').val(data.alamat);
                    $('#kotaLahir').val(data.kota_lahir);
                    $('#tglLahir').val(data.tanggal_lahir);
                    $('#saldo').val(formated);
                } else {
                    setBatal();
                    Swal.fire(
                        'Error!',
                        'Saldo Anda Tidak Cukup',
                        'error'
                    );
                }
            },
            error: function(xhr, status, error) {
                console.error({Error: err, Status:  status, xhr: xhr});
            }
      });
    }
    });


    function setBatal () {
        var action = $('#action').val();
        if (action === 'edit') {
            $('#petugasForm')[0].reset();
            $('#action').val('post');
            var id = "KRDT" + Math.random().toString(36).slice(2);
            $('#id_kredit').val(id);
            loadAnggotaList();
        }else {
            $('#petugasForm')[0].reset();
            var id = "KRDT" + Math.random().toString(36).slice(2);
            $('#id_kredit').val(id);
            // $('#action').val('post');
        }
    }


        // Handle cancel button click
        $('#btnBatal').on('click', function() {
            setBatal();
        });
        $('#btnSimpan').on('click', function() {
            try {
                var action = $('#action').val();
                var id_kredit = $('#id_kredit').val();
                var name_anggota = $('#name_anggota').val();
                var nik = $('#nik').val();
                var tlp = $('#tlp').val();
                var status= $('#status').val();
                var kotaLahir = $('#kotaLahir').val();
                var tglLahir = $('#tglLahir').val();
                var alamat = $('#address').val();
                var tgl = $('#tgl').val();
                var saldo = $('#saldo').val().replace(/\./g, '');
                var jumlah_pinjaman = $('#jumlah_pinjaman').val().replace(/\./g, '');
                var admin = $('#admin').val().replace(/\./g, '');
                var tenor = $('#tenor').val();
                var angsuran = $('#angsuran').val().replace(/\./g, '');
                if (action === 'post') {
                    var formData = {
                        id_kredit: id_kredit,
                        id_anggota: name_anggota,
                        nik: nik,
                        telpon: tlp,
                        kota_lahir: kotaLahir,
                        tanggal_lahir: tglLahir,
                        alamat: alamat,
                        tgl: tgl,
                        saldo: Number(saldo),
                        jumlah_pinjaman: Number(jumlah_pinjaman),
                        admin: Number(admin),
                        tenor: parseInt(tenor),
                        angsuran: Number(angsuran),
                        status: status
                    };
                    $.ajax({
                        url: 'http://localhost:1199/koperasi/app/controller/add-pinjaman-anggota.php',
                        type: 'POST',
                        data: formData,
                        success: function(response) {
                                var data = JSON.parse(response);
                                setBatal();
                                    if (data.success) {
                                        Swal.fire(
                                            'Success!',
                                            data.message,
                                            'success'
                                        );
                                    } else {
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Failed add new record',
                                            text: data.message
                                        });
                                    }
                            },
                        error: function(xhr, status, error) {
                            setBatal();
                            Swal.fire(
                                'Error!',
                                'Failed to save data: ' + xhr.responseText,
                                'error'
                            );
                        }
                    });
                }
            } catch (error) {
                console.error('Error:', err);
            }
        });
});